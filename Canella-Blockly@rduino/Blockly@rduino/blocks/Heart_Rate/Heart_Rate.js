/**
 * Visual Blocks for Track Location Blocks
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

 'use strict';

 goog.provide('Blockly.Blocks.Heart_Rate');
 
 goog.require('Blockly.Blocks');
 goog.require('Blockly.Types');


 Blockly.Blocks['Heart_Rate'] = {
   
    init: function() {
  
      this.appendValueInput("PIN")
      .setCheck('String')
          .appendField("Measure Heart Rate       ")
          .appendField(new Blockly.FieldImage(Blockly.pathToBlockly + 'blocks/Heart_Rate/Heart_Rate.png', Blockly.Arduino.imageSize, Blockly.Arduino.imageSize))
          .appendField("Port")
          this.appendStatementInput("Name")
          .setCheck(null);
      this.setPreviousStatement(true,null);    
      this.setNextStatement(true, null);
      this.setColour("#FF5F5D");

      
      this.setTooltip("This Block starts receiving the heart rate using Finger-clip Heart Rate Sensor with shell.");
      this.setHelpUrl("https://wiki.seeedstudio.com/Grove-Finger-clip_Heart_Rate_Sensor_with_shell/");
      
    }
  };
  
  Blockly.Blocks['Measure_Heart_Rate'] = {
   
    init: function() {
      this.appendValueInput("Measure_Heart_Rate")
      .setCheck(null)
          .appendField("       Retrieve Heart Rate          ")
      this.setPreviousStatement(true,null);    
      this.setNextStatement(true, null);
      this.setColour("#34A56F");

      
      this.setTooltip("Warning !!! This Block collects sensitive data. It measures heart rate using Finger-clip Heart Rate Sensor with shell.");
      this.setHelpUrl("https://wiki.seeedstudio.com/Grove-Finger-clip_Heart_Rate_Sensor_with_shell/");
      
    },

        onchange: function(){

     
          var connected = this.getInput('Measure_Heart_Rate').connection.targetBlock() !== null;
          if (!connected)
     {
      this.setWarningText("This block collects sensitive data. \n" +
      "Are you sure you want to send or store the heart rate values in their raw format? \n" +
      " Minimise Raw Data Intake block or Category-Bsed Aggregation block is suggested to comply with Privacy and Data Protection Laws.");
     }

     else if (this.getInputTargetBlock('Measure_Heart_Rate').getFieldValue('Minimise_Raw')  || this.getInputTargetBlock('Measure_Heart_Rate').getFieldValue('Category_Based_Aggregation'))
     {
      this.setWarningText(null);
     }
    }
    
  };

       

  Blockly.Blocks['Print_Heart_Rate_Minitue'] = {
    /**
  * Block for creating a list with any number of elements of any type.
  * @this Blockly.Block
  */
  init: function() {
  this.appendDummyInput("")
      .appendField("         Display Heart Rate          ")
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#34A56F");
  this.setTooltip("This block dispaly the heart rate beat per minute during a particular workout ");
  this.setHelpUrl("https://www.arduino.cc/reference/en/");
  }
  };




  Blockly.Blocks['Measure_Active_Calories'] = {
    
    /* #F28705 */
     init: function() {
   
      this.appendValueInput("Measure_Active_Calories")
      .setCheck(null)
           .appendField("     Measure Active calories     ")
           this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
           this.setColour("#FFB73C");
       this.setTooltip("warning !! This Block measure the active calories during a particular workout, based on the age and the gender of the usr. It collects sensitive data ");
       this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
     },

     onchange: function(){
        
     /* if (this.Connection != null)
      {
       this.setWarningText(null);
      }*/
                   
       var connected = this.getInput('Measure_Active_Calories').connection.targetBlock() !== null;
      if (!connected)  
      {
        this.setWarningText("This block collects sensitive data. \n" +
        "Are you sure you want to send or store the active calories in their raw format? \n" +
        "Category-Bsed Aggregation block is suggested to comply with Privacy and Data Protection Laws.");
      }
 
      else if (this.getInputTargetBlock('Measure_Active_Calories').getFieldValue('Category_Based_Aggregation'))
      {
        this.setWarningText(null);
      }
     }
   };

  
  Blockly.Blocks['Print_Active_Calories'] = {
    /**
  * Block for creating a list with any number of elements of any type.
  * @this Blockly.Block
  */
  init: function() {
  this.appendDummyInput("")
      .appendField("      Display Active Calories     ")
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#FFB73C");
  this.setTooltip("This block dispaly the active calories during a particular workout ");
  this.setHelpUrl("https://www.arduino.cc/reference/en/");
  }
  
  };
  Blockly.Blocks['Print_Total_Calories'] = {
    /**
  * Block for creating a list with any number of elements of any type.
  * @this Blockly.Block
  */
  init: function() {
  this.appendDummyInput("")
      .appendField("      Retrieve total calories for a workout     ")
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#FFB73C");
  this.setTooltip("This block dispaly the total calories for a particular workout ");
  this.setHelpUrl("https://www.arduino.cc/reference/en/");
  }
  
  };
  

  
Blockly.Blocks['Print_Lists_Heart_Rate_Minitue'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
this.appendDummyInput("")
    .appendField("Retrieve heart rate values for a workout")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#FF5F5D");
this.setTooltip("This block retrieves the heart rate beat readings after completing a particular workout ");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
}
};


