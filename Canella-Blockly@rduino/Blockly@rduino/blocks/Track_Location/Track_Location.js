/**
 * Visual Blocks for Track Location Blocks
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

 'use strict';

 goog.provide('Blockly.Blocks.Track_Location');
 
 goog.require('Blockly.Blocks');
 goog.require('Blockly.Types');


Blockly.Blocks['Track_Location'] = {
  init: function() {
      
    this.appendValueInput("PIN2")
    .setAlign(Blockly.ALIGN_RIGHT)
    .setCheck('Number')
    .appendField("PIN# 1")
    
     this.appendValueInput("PIN3")
     .setAlign(Blockly.ALIGN_RIGHT)
     .setCheck('Number')
     
        .appendField("Track User's Location")
        .appendField(new Blockly.FieldImage(Blockly.pathToBlockly + 'blocks/Track_Location/Track_Location.png', Blockly.Arduino.imageSize, Blockly.Arduino.imageSize))
        .appendField("PIN# 2")
        this.appendStatementInput("Name")
        .setCheck(null);

    this.setPreviousStatement(true, null);
    this.setNextStatement(true,null);
    this.setColour("#008B8B");
    this.setTooltip("This block initializes the connection to a GPS module to provides a precise, sensitive, and low-power GPS module for the user's position recognition.");
    this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
  }
};


Blockly.Blocks['Date'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
this.appendDummyInput("")
    .appendField("            Retireve Date              ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
this.setColour("#E1523D");
this.setTooltip("This block retrieves the latest date fix (UT) using a tinyGPS library  \n" +
"It block collects personal data.\n" +
"Are you sure you want to share or store the date information when a particular workout is performed?");
this.setHelpUrl("https://www.arduino.cc/reference/en/libraries/tinygps/");
},

onchange: function(){


  if (this.nextConnection.targetBlock() == null)
  {
  this.setWarningText("This block collects personal data. \n" +
  "Are you sure you want to share or store the date information when a particular workout is performed? \n" +
  "If you can create your application without collecting date information, remove it from your workspace.\n" +
  "However, if it is required by your application, connect it to the flow of the blocks.");
  }
  else if ((this.nextConnection.targetBlock().type == 'Minimise_Storage'))
  {
  this.setWarningText(null);
  }
  }
  };

Blockly.Blocks['Print_Date'] = {
  /* #F28705 */
   init: function() {
    this.appendDummyInput().appendField("            Display Date                ");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#E1523D");
     this.setTooltip("This block displays the latest date fix (UT) to the Arduino serial monitor.");
     this.setHelpUrl("https://www.arduino.cc/reference/en/libraries/tinygps/");
   }
 };
 Blockly.Blocks['Current_Time'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
  this.appendDummyInput("")
    .appendField("        Retireve Current Time     ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
this.setColour("#F4AE68");
this.setTooltip("This block retrieves the latest time fix (UT) using a tinyGPS library \n" +
"It collects personal data. \n" +
  "Are you sure you want to share or store the current time information when a particular workout is performed");
this.setHelpUrl("https://www.arduino.cc/reference/en/libraries/tinygps/");
},

onchange: function(){


  if (this.nextConnection.targetBlock() == null)
  {
  this.setWarningText("This block collects personal data. \n" +
  "Are you sure you want to share or store the current time information when a particular workout is performed? \n" +
  "If you can create your application without collecting the current time information, remove it from your workspace. \n" +
  "However, if it is required by your application, connect it to the flow of the blocks.");
  }
  else if ((this.nextConnection.targetBlock().type == 'Minimise_Storage'))
  {
  this.setWarningText(null);
  }
  }
  };

  
Blockly.Blocks['Print_Current_Time'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
    this.appendDummyInput("").appendField("        Display Current Time       ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
this.setColour("#F4AE68");
this.setTooltip("This block displays the latest time fix (UT) to the Arduino serial monitor. ");
this.setHelpUrl("https://www.arduino.cc/reference/en/libraries/tinygps/");
}
};

Blockly.Blocks['Time'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
  this.appendValueInput("Time")
  .setCheck(null)
    .appendField("  Retireve Activity Spent Time  ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#34A56F");
this.setTooltip("This block retrieves the activity spent time using a tinyGPS library. \n" +
"It collects personal data. \n" +
  "Are you sure you want to share or store the activity spent time information when a particular workout is performed?");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
},

onchange: function(){

  var connected = this.getInput('Time').connection.targetBlock() !== null;
  if (!connected)

  {
  this.setWarningText("This block collects personal data. \n" +
  "Are you sure you want to share or store the activity spent time information when a particular workout is performed? \n" +
  "Category-Bsed Aggregation block is suggested to comply with Privacy and Data Protection Laws.");
  }
  else if (this.getInputTargetBlock('Time').getFieldValue('Category_Based_Aggregation'))
  {
  this.setWarningText(null);
  }
  }
  };


Blockly.Blocks['Print_Time'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
    this.appendDummyInput("").appendField("   Display Activity Spent Time   ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#34A56F");
this.setTooltip("This block displays the activity spent time to the Arduino serial monitor.");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
}
};



Blockly.Blocks['LAT_LNG'] = {
 /* #F28705 */
  init: function() {
   // this.appendDummyInput().appendField("Retrieve Longtitude and Latitude")
    this.appendValueInput("LAT_LNG")
    .setCheck(null)
        .appendField("Retrieve Longtitude and Latitude")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#AD5064");
    this.setTooltip("Warning !! This Block collects personal data. It retrieves the latest altitude fix and latest longitude fix of the user's position.");
    this.setHelpUrl("https://www.arduino.cc/reference/en/libraries/tinygps/");
  },

    onchange: function(){

      var connected = this.getInput('LAT_LNG').connection.targetBlock() !== null;
     if (!connected)
      {
        this.setWarningText("This block collects personal data (fine-grained location). \n" +
        "Can you use coarse-grained location data instead? \n" +
        "Reduce Location Granularity block is suggested to comply with Privacy and Data Protection Laws");
      }
      if (this.getInputTargetBlock('LAT_LNG').getFieldValue('Location_Granularity'))
     {
      this.setWarningText(null);
     }


     
    }

};

Blockly.Blocks['Print_Location'] = {
  /* #F28705 */
   init: function() {
    this.appendDummyInput().appendField("  Display Longtitude and Latitude  ");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#AD5064");
     this.setTooltip("This block displays the latest altitude fix and latest longitude fix to the Arduino serial monitor.");
     this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
   }
 };

 Blockly.Blocks['Print_Lists_Locations'] = {
  /* #F28705 */
   init: function() {
    this.appendDummyInput().appendField("Retrieve latitudes and longitudes for a workout");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#AD5064");
     this.setTooltip("This block displays the values of the latitudes and longitudes during a workout using a tinyGPS library");
     this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
   }
 };

Blockly.Blocks['Speed'] = {
  /**
* Block for creating a list with any number of elements of any type. 4C6E5E
* @this Blockly.Block
*/
init: function() {
  this.appendValueInput("Speed")
  .setCheck(null)
    .appendField("           Retrieve Speed             ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
this.setColour("#769897");
this.setTooltip("Warning !!! This block retrieves the current ground speed in kilometers per hour during a workout using a tinyGPS library \n" +
"It collects personal data");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
},

onchange: function(){

     
  var connected = this.getInput('Speed').connection.targetBlock() !== null;
  if (!connected)
  {
    this.setWarningText("This block collects personal data. \n" +
    "Are you sure you want to send or store the speed values in their raw format? \n" +
    " Minimise Raw Data Intake block or Category-Bsed Aggregation block is suggested to comply with Privacy and Data Protection Laws.");
    }
  
    else if (this.getInputTargetBlock('Speed').getFieldValue('Minimise_Raw') || this.getInputTargetBlock('Speed').getFieldValue('Category_Based_Aggregation'))
 {
  this.setWarningText(null);
 }
}
};

Blockly.Blocks['Print_Speed'] = {
  /* #F28705 */
   init: function() {
    this.appendDummyInput("").appendField("            Display Speed             ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
         this.setColour("#769897");
     this.setTooltip("This block displays the current ground speed in kilometers per hour to the Arduino serial monitor.");
     this.setHelpUrl("https://www.arduino.cc/reference/en/");
   }
 };


 Blockly.Blocks['Print_Lists_Speed'] = {
  /* #F28705 */
   init: function() {
    this.appendDummyInput("").appendField("       Retrieve speed values for a workout      ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
         this.setColour("#769897");
     this.setTooltip("This block displays the values of the current ground speed in kilometers per hour to the Arduino serial monitor.");
     this.setHelpUrl("https://www.arduino.cc/reference/en/");
   }
 };


Blockly.Blocks['Distance'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
  this.appendValueInput("Distance")
  .setCheck(null)
    .appendField("            Retrieve Distance        ")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
this.setColour("#A5A692");
this.setTooltip("Warning. This block retrieves the distance. It collectes personal data. It collects personal data");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
},

onchange: function(){
   
  var connected = this.getInput('Distance').connection.targetBlock() !== null;
  if (!connected)

  {
    this.setWarningText("This block collects personal data. \n" +
    "Are you sure you want to send or store the Distance values in their raw format? \n" +
    "Category-Bsed Aggregation block is suggested to comply with Privacy and Data Protection Laws.");
  }

  else if (this.getInputTargetBlock('Distance').getFieldValue('Minimise_Raw') || this.getInputTargetBlock('Distance').getFieldValue('Category_Based_Aggregation'))
  {
  this.setWarningText(null);
  }
  }
  };

Blockly.Blocks['Print_Distance'] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
  this.appendDummyInput("").appendField("            Display Distance         ")
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
this.setColour("#A5A692");
this.setTooltip("This block displays the distance to the Arduino serial monitor.");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
}
};

Blockly.Blocks["Print_Total_Distance"] = {
  /**
* Block for creating a list with any number of elements of any type.
* @this Blockly.Block
*/
init: function() {
  this.appendDummyInput("").appendField("   Retrieve total distance for a workout         ")
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
this.setColour("#A5A692");
this.setTooltip("This block retrievs the total distance for a particular workout.");
this.setHelpUrl("https://www.arduino.cc/reference/en/");
}
};

