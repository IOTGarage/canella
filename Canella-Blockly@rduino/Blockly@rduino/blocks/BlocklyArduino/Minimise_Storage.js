/**
 *
 *
 * Copyright 2022 Atheer Aljeraisy.
 * http://greich.fr
 *
 * This work is licensed under a Creative Commons Attribution 4.0 International.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 */

 'use strict';

goog.provide('Blockly.Blocks.Minimise_Storage');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

Blockly.Blocks['Minimise_Storage'] = {
  init: function() {
    this.appendDummyInput("")
        .appendField("Minimize Data Storage and Retention Period")
        .appendField(new Blockly.FieldDropdown([
            ["Date", "Date"],
            ["Current Time", "Current_Time"],
            ["Activity Spent Time", "Activity_Spent_Time"],
            ["Location", "Location_Data"],
            ["Heart Rate", "Heart_Rate"],
            ["Total Calories", "Total_Calories"],
            ["Speed", "Speed"],
            ["Distance", "Distance"]
        ]), "Store_Data");
    
    this.appendDummyInput()
        .appendField("Retention Period (days)")
        .appendField(new Blockly.FieldNumber(30, 1, Infinity, 1), "Retention_Period");

    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#3CB371");

    this.setTooltip(
      "- This block minimizes the amount of data stored by an IoT application and the duration for which data is stored. It helps avoid retaining data for longer than needed.\n" +
      "- Any piece of data that is not required to perform a certain task should be deleted. For example, raw data can be deleted once secondary contexts are derived.\n" +
      "- Long retention periods provide more time for malicious parties to attempt accessing the data in an unauthorized manner.\n" +
      "- This block uses EPROM memory in the Arduino to update the stored primary data with the secondary data after it has been derived by integrating one of the privacy block.\n\n" +
      "** Guidance:\n" +
      "1. Select the data to be updated with the new derived data.\n" +
      "2. Insert the retention period as a number of days.\n\n" +
      "** Compliance with Privacy and Data Protection Laws:\n" +
      "1. Data Minimization Principle\n" +
      "2. Storage Limitation Principle"
  );
    this.setHelpUrl("");
  }
};
