/**
 * Visual Blocks for Print Block
 *
 * Copyright 2022 Atheer Aljeraisy.
 * http://greich.fr
 *
 * This work is licensed under a Creative Commons Attribution 4.0 International.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 */

 Blockly.Blocks['Print_Total_Heart_Rate'] = {
    /**
  * Block for creating a list with any number of elements of any type.
  * @this Blockly.Block
  */
  init: function() {
  this.appendDummyInput("")
      .appendField("      Print Total Heart Rate     ")
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
  this.setColour("#025E73");
  this.setTooltip("This block..");
  this.setHelpUrl("https://www.arduino.cc/reference/en/");
  }
  };