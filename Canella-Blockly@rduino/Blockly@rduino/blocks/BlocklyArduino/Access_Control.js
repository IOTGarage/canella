'use strict';

goog.provide('Blockly.Blocks.Access_Control');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

// Data Access Control Block
Blockly.Blocks['Data_Access_Control'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("                     Access Control               ");
        this.appendStatementInput('Name')
            .setCheck(null);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#D9843B");
        this.setTooltip("Access Control ensures that information is processed legitimately and by an authorized party to prevent an activity that could lead to a security breach. \n" +
            "** Guidance: \n" +
            "1. Connect the Authenticate User block to verify user identity. \n" +
            "2. Connect the Authorize User block to grant appropriate permissions for their actions.\n" +
            "** Compliance with the Privacy and Data Protection Laws: \n" +
            "1. Security Principle.");
        this.setHelpUrl("https://gdpr-info.eu/art-5-gdpr/");
    }
};

// Authenticate User Block
Blockly.Blocks['authenticate_user'] = {
    init: function() {
        var userTypes = [
            ["Data Subject", "Data Subject"],
            ["Controller", "Controller"],
            ["Processor", "Processor"],
            ["Third Party", "Third Party"]
        ];

        var passwordTypes = [
            ["Alphanumeric Password", "Password"],
            ["Biometric Password", "BiometricPassword"],
            ["Token-based Password", "TokenBasedPassword"],
            ["ID-based Password", "IDPassword"],
            ["Face Recognition Password", "FaceRecognitionPassword"],
            ["Fingerprint Password", "FingerprintPassword"]
        ];

        this.appendDummyInput()
            .appendField("Authenticate User")
            .appendField(new Blockly.FieldDropdown(userTypes, this.updateTooltip.bind(this)), "User_Types");

        for (var i = 0; i < passwordTypes.length; i++) {
            this.appendDummyInput()
                .appendField(new Blockly.FieldCheckbox("FALSE", this.updateTooltip.bind(this)), passwordTypes[i][1])
                .appendField(passwordTypes[i][0]);
        }

        this.setPreviousStatement(true, null);
        this.setNextStatement(true, 'authorize_user');
        this.setColour("#052D49");
        this.setTooltip("Authenticate a user using selected user type and password types.");
        this.setHelpUrl("");
    },

    updateTooltip: function() {
        var userType = this.getFieldValue('User_Types');
        var passwordTypeTooltips = {
            "Password": "Alphanumeric Password: Uses both letters and numbers for a balanced mix of security and usability.",
            "BiometricPassword": "Biometric Password: Uses unique biological characteristics like fingerprints or facial features for verification.",
            "TokenBasedPassword": "Token-based Password: Uses a device to generate secure, one-time passwords synchronized with a server.",
            "IDPassword": "ID-based Password: Authenticates based on facial features compared to stored templates.",
            "FaceRecognitionPassword": "Face Recognition Password: Utilizes facial features of an individual for authentication.",
            "FingerprintPassword": "Fingerprint Password: Authenticates using a person's unique fingerprint pattern."
        };

        var userTypeTooltip = {
            "Data Subject": "Data Subject: An individual whose personal data is collected and processed.",
            "Controller": "Controller: An entity that determines the purposes and means of processing personal data.",
            "Processor": "Processor: An entity that processes data on behalf of the controller, following their instructions.",
            "Third Party": "Third Party: An entity that interacts with or processes data under certain conditions, often providing additional services or support."
        };

        var tooltip = "Authenticate a user using selected user type and password types.\n\n" + userTypeTooltip[userType] + "\n\nPassword Types:\n";
        for (var key in passwordTypeTooltips) {
            if (this.getFieldValue(key) === 'TRUE') {
                tooltip += passwordTypeTooltips[key] + "\n";
            }
        }

        this.setTooltip(tooltip);
    }
};

// Authorize User Block
Blockly.Blocks['authorize_user'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Authorize User:           ");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Read")
            .appendField("Read");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Write")
            .appendField("Write");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Delete")
            .appendField("Delete");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Download")
            .appendField("Download");

        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#86A69D");
        this.setTooltip(function() {
            let tooltipText = "Authorize a user for specific actions:\n\n";
            tooltipText += "Read: Grants permission to view or access data.\n";
            tooltipText += "Write: Grants permission to modify or add new data.\n";
            tooltipText += "Delete: Grants permission to remove data.\n";
            tooltipText += "Download: Grants permission to download data to a local device.\n";
            return tooltipText;
        });
        this.setHelpUrl("");
    }
};

/*
// Log Action Block (Commented Out)
Blockly.Blocks['log_action'] = {
    init: function() {
        this.appendValueInput('ACTION')
            .setCheck('String')
            .appendField('Log action');
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(160);
        this.setTooltip('Logs an action with user identification.');
        this.setHelpUrl('https://your.documentation.url/log_action');
    }
};

Blockly.JavaScript['log_action'] = function(block) {
    var action = Blockly.JavaScript.valueToCode(block, 'ACTION', Blockly.JavaScript.ORDER_ATOMIC);
    var user = 'getUserIdentifier()';  // Example function to retrieve user identifier
    var code = 'console.log("User: " + ' + user + ' + " did: " + ' + action + ');\n';
    return code;
};
*/

/*
// Access Control Block (Commented Out)
Blockly.Blocks['Access_Control'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Access Control");
        this.appendDummyInput()
            .appendField("User Types")
            .appendField(new Blockly.FieldCheckbox("Data_Subject"), "Data_Subject")
            .appendField("Data Subject")
            .appendField(new Blockly.FieldCheckbox("Controller"), "Controller")
            .appendField("Controller")
            .appendField(new Blockly.FieldCheckbox("Processor"), "Processor")
            .appendField("Processor")
            .appendField(new Blockly.FieldCheckbox("Third_Party"), "Third_Party")
            .appendField("Third Party");
        this.appendDummyInput()
            .appendField("Password Types")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Alphanumeric_Password")
            .appendField("Alphanumeric Password");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Biometric_Password")
            .appendField("Biometric Password");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Token_Based_Password")
            .appendField("Token-based Password");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "ID_Based_Password")
            .appendField("ID-based Password");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Face_Recognition_Password")
            .appendField("Face Recognition Password");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Fingerprint_Password")
            .appendField("Fingerprint Password");
        this.appendDummyInput()
            .appendField("Permissions")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Read")
            .appendField("Read");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Write")
            .appendField("Write");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Delete")
            .appendField("Delete");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "Download")
            .appendField("Download");

        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#D9843B");
        this.setTooltip("This block ensures that information is processed legitimately and by an authorized party to prevent an activity that could lead to a security breach.");
        this.setHelpUrl("");
    }
};
*/

