'use strict';

goog.provide('Blockly.Blocks.encrypt_and_send');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

Blockly.Blocks['encrypt_and_send_data'] = {
  init: function() {
    var protocols = [
      ["HTTPS", "HTTPS"],
      ["TLS", "TLS"],
      ["SSL", "SSL"]
    ];

    this.appendDummyInput()
        .appendField("Encrypted Data Communication")
        .appendField(new Blockly.FieldDropdown(protocols, this.updateTooltip.bind(this)), "PROTOCOL");

    this.appendValueInput("URL")
        .setCheck("String")
        .appendField("Destination URL");

    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#0b6685");

    // Set initial tooltip
    this.setTooltip(
      "This block encrypts and sends data using a specified protocol. Encrypted data communication reduces potential privacy risks due to unauthorized access during data transfer.\n\n" +
      "** Guidance:\n" +
      "1. Choose the appropriate data communication protocol for secure communication.\n" +
      "2. Insert the destination URL where you are going to share data.\n\n" +
      "** Compliance with Privacy and Data Protection Laws:\n" +
      "1. Security Principle"
    );

    this.setHelpUrl("");
  },

  // Update tooltip based on selected protocol
  updateTooltip: function(option) {
    var tooltips = {
      "HTTPS": "HTTPS (Hypertext Transfer Protocol Secure): Use HTTPS for web-based communication where data security is essential. Suitable for most IoT applications sending data over the internet.",
      "TLS": "TLS (Transport Layer Security): Ensures secure communication over a computer network. Use TLS for secure, low-level communication between devices or when setting up encrypted tunnels.",
      "SSL": "SSL (Secure Sockets Layer): Deprecated predecessor to TLS for secure communication. Generally, TLS should be preferred over SSL due to its improved security features."
    };

    var tooltip = tooltips[option];
    if (tooltip) {
      this.setTooltip(tooltip);
    } else {
      this.setTooltip(
        "This block encrypts and sends data using a specified protocol. Encrypted data communication reduces potential privacy risks due to unauthorized access during data transfer.\n\n" +
        "** Guidance:\n" +
        "1. Choose the appropriate data communication protocol for secure communication.\n" +
        "2. Insert the destination URL where you are going to share data.\n\n" +
        "** Compliance with Privacy and Data Protection Laws:\n" +
        "1. Security Principle"
      );
    }
  }
};
