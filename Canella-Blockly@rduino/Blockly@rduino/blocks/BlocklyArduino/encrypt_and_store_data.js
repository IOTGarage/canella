'use strict';

goog.provide('Blockly.Blocks.encrypt_and_store_data');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

Blockly.Blocks['encrypt_and_store_data'] = {
  init: function() {
    this.appendDummyInput("")
        .appendField("Encrypted Data Storage")
        .appendField(new Blockly.FieldDropdown([
          ["Symmetric", "SYMMETRIC"],
          ["Asymmetric", "ASYMMETRIC"]
        ], this.updateTechniqueOptions.bind(this)), "ENCRYPTION_TYPE");
    this.appendDummyInput("")
        .appendField("Encryption Technique")
        .appendField(new Blockly.FieldDropdown([["Select a type first", ""]]), "TECHNIQUE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);

    this.setColour("#3E8C84");
    this.setTooltip("- This block suggests encryption techniques based on type. \n" +
    "- IoT applications should encrypt data to prevent privacy violations and unauthorized access.\n" +
    "** Recoomendations: \n" +
    "Symmetric encryption techniques are generally preferred for IoT applications, especially when using microcontrollers, \n"+
     "due to their efficiency and lower computational overhead compared to asymmetric encryption. \n"+
     "** Compliance with the Privacy and Data Protection Laws: \n" +
     "1. Security Principle.");
    this.setHelpUrl("");
  },

  updateTechniqueOptions: function(encryptionType) {
    const symmetricTechniques = [
      ["AES (Block Cipher)", "AES"],
      ["DES (Block Cipher)", "DES"],
      ["Triple DES (Block Cipher)", "3DES"],
      ["Twofish (Block Cipher)", "TWOFISH"],
      ["Trivium (Stream Cipher)", "TRIVIUM"]
    ];

    const asymmetricTechniques = [
      ["RSA", "RSA"],
      ["PKI (Public Key Infrastructure)", "PKI"]
    ];

    const techniqueField = this.getField("TECHNIQUE");

    if (encryptionType === "SYMMETRIC") {
      techniqueField.menuGenerator_ = symmetricTechniques;
    } else if (encryptionType === "ASYMMETRIC") {
      techniqueField.menuGenerator_ = asymmetricTechniques;
    } else {
      techniqueField.menuGenerator_ = [["Select a type first", ""]];
    }

    techniqueField.setValue(techniqueField.menuGenerator_[0][1]);
    this.updateTooltip(techniqueField.menuGenerator_[0][1]);

    // Add event listener to update tooltip when technique is changed
    techniqueField.setValidator(this.updateTooltip.bind(this));
  },

  updateTooltip: function(technique) {
    let tooltipText = "";
    switch (technique) {
      case 'AES':
        tooltipText = "AES (Block Cipher): Advanced Encryption Standard, known for its strong security and efficiency.";
        break;
      case 'DES':
        tooltipText = "DES (Block Cipher): Data Encryption Standard, an older symmetric-key method for data encryption.";
        break;
      case '3DES':
        tooltipText = "Triple DES (Block Cipher): An enhancement of DES that applies the encryption algorithm three times.";
        break;
      case 'TWOFISH':
        tooltipText = "Twofish (Block Cipher): A symmetric key block cipher known for its speed and flexibility.";
        break;
      case 'TRIVIUM':
        tooltipText = "Trivium (Stream Cipher): A lightweight stream cipher designed for efficiency in hardware.";
        break;
      case 'RSA':
        tooltipText = "RSA: A widely-used asymmetric encryption algorithm that relies on the computational difficulty of factoring large integers.";
        break;
      case 'PKI':
        tooltipText = "PKI (Public Key Infrastructure): A framework for creating a secure method for exchanging information using public key cryptography.";
        break;
      default:
        tooltipText = "Select a technique to see its description.";
        break;
    }
    this.setTooltip(tooltipText);
    return technique;  // Ensure the technique value is returned correctly
  }
};

Blockly.JavaScript['encrypt_and_store_data'] = function(block) {
  var encryptionType = block.getFieldValue('ENCRYPTION_TYPE');
  var technique = block.getFieldValue('TECHNIQUE');
  var code = 'Selected encryption type: ' + encryptionType + ', Technique: ' + technique + ';\n';
  return code;
};
