/**
 * Visual Blocks for Retrieving Workout Data
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

 Blockly.Blocks['Retrieve_Data'] = {
    /**
  * Block for creating a list with any number of elements of any type.
  * @this Blockly.Block
  */
  init: function() {
    this.appendDummyInput("")
      .appendField("          Retrieve Workout Data          ")
      this.appendStatementInput('Name')
          .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
  this.setColour("#A5A692");
  this.setTooltip("This block specifies a condition for the end of a workout interval so that workout values can be retrieved after the workout is done.");
  this.setHelpUrl("https://www.arduino.cc/reference/en/");
  }
  }; 