/**
 * Visual Blocks for Track Location Blocks
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

'use strict';

goog.provide('Blockly.Blocks.end_fitness');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

Blockly.Blocks['end_fitness'] = {
          /**
     * Block for creating a list with any number of elements of any type.
     * @this Blockly.Block
     */
  init: function() {
    this.appendDummyInput().appendField("End Fitness Tracking IoT Application");
    this.setPreviousStatement(true, null);
     this.setColour("#024959");
     this.setTooltip("This block shows the end of the sequence of the Fitness Tracking IoT Application blocks.");
     this.setHelpUrl("");
}
};
