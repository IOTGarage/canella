'use strict';

goog.provide('Blockly.Blocks.Minimise_Raw');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

Blockly.Blocks['Minimise_Raw'] = {
  /**
    * Block for creating a list with any number of elements of any type.
    * @this Blockly.Block
    */
 init: function() {
   
  var typeOptions = [['Heart Rate', 'Heart_Rate'], ['Speed', 'Speed']];
   this.appendDummyInput("Minimize Raw Data Intake")
       .appendField("Minimize Raw Data Intake")
       .appendField(new Blockly.FieldDropdown(typeOptions, this.handleTypeSelection.bind(this)), 'Minimise_Raw');
      // Initialize the value of this.columnType (used in updateShape)
      this.columnType = this.getFieldValue('typeSelector');
      // Avoid duplicating code by running updateShape to append your appropriate input
      this.updateShape();
      //@TODO: Do other block configuration stuff like colors, additional inputs, etc. here

       this.setOutput(true, 'string');
   this.setColour('#008B8B');
   this.setTooltip(
    "- This block reduces the amount of raw data it receives by calculating an average of the sensor data values over a specified period of time.\n" +
    "- It reduces the granularity of data and also minimizes secondary usage that could lead to privacy violations.\n\n" +
    "** Guidance:\n" +
    "1. Select the data to apply the Minimize Raw Data Intake block to.\n" +
    "2. Choose the units of time (seconds, minutes, hours) for data minimization.\n" +
    "3. Insert a number value for the time period to perform the averaging, e.g., return the average sensor data values every 5 seconds.\n\n" +
    "** Compliance with Privacy and Data Protection Laws:\n" +
    "1. Data Minimization Principle. \n" +
    "2. Limiting Use and Disclosure.\n" +
    "3. Storage Limitation Principle."
);
this.setHelpUrl("");
 },


    /**
     * This function runs each time you select a new value in your type selection dropdown field.
     * @param {string} newType This is the new value that the field will be set to.
     * 
     * Important note: this function will run BEFORE the field's value is updated. This means that if you call
     * this.getFieldValue('typeSelector') within here, it will reflect the OLD value.
     * 
     */
     handleTypeSelection: function (newType) {
      // Avoid unnecessary updates if someone clicks the same field twice
      if(this.columnType !== newType) {
          // Update this.columnType to the new value
          this.columnType = newType;
          // Add or remove fields as appropriate
          this.updateShape();
      }
  },
  /**
   * This will remove old inputs and add new inputs as you need, based on the columnType value selected
   */
  updateShape: function () {
    var viewsList = [["Seconds","Seconds"], ["Minutes","Minutes"], ["Hours","Hours"]];
      // Remove the old input (so that you don't have inputs stack repeatedly)
      if (this.getInput('appendToMe')) {
          this.removeInput('appendToMe');
      }
      // Append the new input based on the value of this.columnType
      if(this.columnType === 'Heart_Rate') {
          // if columnType = Heart_Rate, show the following:
          //@TODO: define values in cardsList here
          this.appendDummyInput('appendToMe')
              .appendField('Average Heart Rate per Minitues: ')
              .appendField('Every')
              .appendField(new Blockly.FieldNumber(5), 'Value')
              .appendField(new Blockly.FieldDropdown(viewsList), 'Time');
      }
       else if (this.columnType === 'Speed') {
          //  if columnType = view, show the following:
          //@TODO: define values in viewsList here
          this.appendDummyInput('appendToMe')
              .appendField(' Average Speed per KM:')
              .appendField('Every')
              .appendField(new Blockly.FieldNumber(5), 'Value')
              .appendField(new Blockly.FieldDropdown(viewsList), 'Time');
      }
  }
};
