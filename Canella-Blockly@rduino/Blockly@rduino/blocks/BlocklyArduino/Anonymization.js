Blockly.Blocks['Anonymization'] = {
  init: function () {
    var typeOptions = [
      ['First Name', 'First Name'],
      ['Last Name', 'Last Name'],
      ['Age', 'Age'],
      ['Date of Birth', 'Date of Birth'],
      ['Gender', 'Gender'],
      ['Height', 'Height'],
      ['Weight', 'Weight'],
      ['Disability', 'Disability'],
      ['Medical Records', 'Medical Records'],
      ['Telephone Number', 'Telephone Number'],
      ['Date', 'Date'],
      ['Current Time', 'Current Time'],
      ['Distance', 'Distance'],
      ['Burned Calories', 'Burned Calories']
    ];

    this.appendDummyInput()
      .appendField("Data Anonymization:")
      .appendField(new Blockly.FieldDropdown(typeOptions, this.handleTypeSelection.bind(this)), 'Data_Anonymization');

    this.setOutput(true, 'String');
    this.setColour("#cb4154");
    this.setTooltip("- This block suggests different data anonymization techniques for anonymizing personally identifiable information before the data gets used by IoT applications.\n" +
     "- Anonymization reduces the risk of unintended disclosure and privacy violations. \n" +
     "** Guidance: \n" +
     "Select the data type and preferred anonymization technique.\n" +
     "** Compliance with the Privacy and Data Protection Laws: \n" +
     "1. Anonymity and Pseudonymity Principle.");
    this.setHelpUrl("");
  },

  handleTypeSelection: function (newType) {
    if (this.columnType !== newType) {
      this.columnType = newType;
      this.updateShape();
    }
  },

  updateShape: function () {
    if (this.getInput('AnonymizedInput')) {
      this.removeInput('AnonymizedInput');
    }

    switch (this.columnType) {
      case 'First Name':
      case 'Last Name':
        this.viewsList = [
          ['Pseudonymization', 'Pseudonymization'],
          ['Data Masking', 'Data Masking'],
          ['Suppression', 'Suppression'],
          ['Generalization', 'Generalization']
        ];
        break;
      case 'Age':
      case 'Date of Birth':
      case 'Height':
      case 'Weight':
      case 'Current Time':
        this.viewsList = [
          ['Categorization', 'Categorization'],
          ['Randomization', 'Randomization'],
          ['Differential Privacy', 'Differential Privacy']
        ];
        break;
      case 'Gender':
      case 'Disability':
        this.viewsList = [['Generalization', 'Generalization']];
        break;
      case 'Telephone Number':
        this.viewsList = [
          ['Data Masking', 'Data Masking'],
          ['Randomization', 'Randomization']
        ];
        break;
      case 'Medical Records':
      case 'Date':
      case 'Distance':
      case 'Burned Calories':
        this.viewsList = [
          ['Randomization', 'Randomization'],
          ['Differential Privacy', 'Differential Privacy']
        ];
        break;
      default:
        this.viewsList = [];
        break;
    }

    if (this.viewsList.length > 0) {
      var dropdown = new Blockly.FieldDropdown(this.viewsList, this.showTooltip.bind(this));
      this.appendDummyInput('AnonymizedInput')
        .appendField('Anonymized ' + this.columnType + ':')
        .appendField(dropdown, this.columnType + '_Value');
      this.showTooltip(dropdown.getValue());
    }
  },

  showTooltip: function (option) {
    var tooltipText = "";
    switch (option) {
      case 'Pseudonymization':
        tooltipText = "Pseudonymization: Replacing private identifiers with fake identifiers or pseudonyms.";
        break;
      case 'Data Masking':
        tooltipText = "Data Masking: Hiding original data with modified content.";
        break;
      case 'Randomization':
        tooltipText = "Randomization: Alters data in a random manner to prevent identification.";
        break;
      case 'Generalization':
        tooltipText = "Generalization: Dilutes the specificity of data.";
        break;
      case 'Suppression':
        tooltipText = "Suppression: Removing or hiding certain data completely.";
        break;
      case 'Differential Privacy':
        tooltipText = "Differential Privacy: Adding random noise to data to protect individual privacy.";
        break;
      case 'Categorization':
        tooltipText = "Categorization: Groups data into broader categories.";
        break;
      default:
        break;
    }
    this.setTooltip(tooltipText);
  }
};
