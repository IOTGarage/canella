
Blockly.Blocks['category_aggregation'] = {
    /**
     * Initiate the block. This runs before domToMutation.
     */
    init: function () {
        var typeOptions = [['Activity Spent Time', 'Activity_Spent_Time'],['Heart Rate', 'Heart_Rate'], ['Active Calories', 'Active_Calories'], ['Distance', 'Distance'], ['Speed', 'Speed']];
        this.appendDummyInput("Category Based Aggregation") 
        .appendField("Category Based Aggregation")
            .appendField(new Blockly.FieldDropdown(typeOptions, this.handleTypeSelection.bind(this)), 'Category_Based_Aggregation');
        // Initialize the value of this.columnType (used in updateShape)
        this.columnType = this.getFieldValue('typeSelector');
        // Avoid duplicating code by running updateShape to append your appropriate input
        this.updateShape();
        //@TODO: Do other block configuration stuff like colors, additional inputs, etc. here

        this.setOutput(true, 'string');
        this.setColour("#F0788C");
        this.setTooltip(
            "This block reduces the granularity of the raw data. \n" +
            "For example, to categorize the heart rate data, the block suggests two categories: status (low, normal, or high), or range (60-80 BPM).\n\n" +
            "** Guidance:\n" +
            "1. Select the data to be categorized.\n" +
            "2. Choose the categories based on the data type. \n" +
            "** Compliance with Privacy and Data Protection Laws:\n" +
            "1. Data Minimization Principle. \n" +
            "2. Limiting Use and Disclosure Principle."
        );
     this.setHelpUrl("");
    },
    /**
     * This function runs each time you select a new value in your type selection dropdown field.
     * @param {string} newType This is the new value that the field will be set to.
     * 
     * Important note: this function will run BEFORE the field's value is updated. This means that if you call
     * this.getFieldValue('typeSelector') within here, it will reflect the OLD value.
     * 
     */
    handleTypeSelection: function (newType) {
        // Avoid unnecessary updates if someone clicks the same field twice
        if(this.columnType !== newType) {
            // Update this.columnType to the new value
            this.columnType = newType;
            // Add or remove fields as appropriate
            this.updateShape();
        }
    },
    /**
     * This will remove old inputs and add new inputs as you need, based on the columnType value selected
     */
    updateShape: function () {
        // Remove the old input (so that you don't have inputs stack repeatedly)
        if (this.getInput('appendToMe')) {
            this.removeInput('appendToMe');
        }
        // Append the new input based on the value of this.columnType
                if(this.columnType === 'Activity_Spent_Time') {
                    // if columnType = Heart_Rate, show the following:
                    //@TODO: define values in cardsList here
                    var viewsList = [['Range','Range']];
                    this.appendDummyInput('appendToMe')
                        .appendField(' Aggregated Activity Spent Time: ')
                        .appendField(new Blockly.FieldDropdown(viewsList), 'Spent_Time_Value');
        }
        // Append the new input based on the value of this.columnType
        if(this.columnType === 'Heart_Rate') {
            // if columnType = Heart_Rate, show the following:
            //@TODO: define values in cardsList here
            var viewsList = [['Status','Status'], ['Range','Range']];
            this.appendDummyInput('appendToMe')
                .appendField(' Aggregated Heart Rate: ')
                .appendField(new Blockly.FieldDropdown(viewsList), 'Heart_Rate_Value');
        }
        else if (this.columnType === 'Active_Calories') {
            //  if columnType = view, show the following:
            //@TODO: define values in viewsList here
            var viewsList = [['Range','Range']];
            this.appendDummyInput('appendToMe')
                .appendField(' Aggregated Active Calories:')
                .appendField(new Blockly.FieldDropdown(viewsList), 'Active_Calories_Value');
        }
         else if (this.columnType === 'Speed') {
            //  if columnType = view, show the following:
            //@TODO: define values in viewsList here
            var viewsList = [['Range','Range']];
            this.appendDummyInput('appendToMe')
                .appendField(' Aggregated Speed ')
                .appendField(new Blockly.FieldDropdown(viewsList), 'Speed_Value');
        }

        else if (this.columnType === 'Distance') {
            //  if columnType = view, show the following:
            //@TODO: define values in viewsList here
            var viewsList = [['Range','Range']];
            this.appendDummyInput('appendToMe')
                .appendField(' Aggregated Distance ')
                .appendField(new Blockly.FieldDropdown(viewsList), 'Distance_Value');
        }
    }
};