﻿'use strict';

goog.provide('Blockly.Blocks.reduce');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

  Blockly.Blocks['reduce'] = {
        /**
     * Privacy Block for Reduce Location Granularity
     * @this Blockly.Block
     */
    init: function() {
      this.appendDummyInput("")
      .appendField("Reduce Location Granularity")
      .appendField(new Blockly.FieldDropdown([["Post Code","Post_Code"], ["City Name","City_Name"], ["Country Name","Country_Name"]]), "Location_Granularity");
      this.appendDummyInput("")
      .appendField("Google Map API key:")
      .appendField(new Blockly.FieldTextInput('API Key'),
          'FIELDNAME');
          this.setOutput(true, 'string');
          this.setColour("#BF665E");
      this.setColour("#F28157");
    this.setTooltip("This block reduces the user's location granularity by performing reverse geocoding to convert geographic coordinates (latitude and longitude) received by the GPS module into a human-readable address (full address).\n" +
    "** Guidence: \n" +
    "1. Select one of the choices for reducing location granularity from full address form to postcode, city name, or country name form. \n" +
    "2. Insert an API key to access Google Map APIs for performing reverse geocoding.\n" +
    "** Compliance with the Privacy and Data Protection Laws: \n" +
    "1. Data Minimization Principle.\n" +
    " 2.Limiting Use and Disclosure Principle.");
    this.setHelpUrl("https://www.arduino.cc/reference/en/");
    }
    };