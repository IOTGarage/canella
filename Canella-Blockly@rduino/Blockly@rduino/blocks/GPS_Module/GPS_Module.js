/**
 * Visual Blocks for GPS_Module
 *
 * Copyright 2022 Atheer Aljeraisy.
 * http://greich.fr
 *
 * This work is licensed under a Creative Commons Attribution 4.0 International.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 */

'use strict';

goog.provide('Blockly.Blocks.GPS_Module');

goog.require('Blockly.Blocks');
goog.require('Blockly.Types');

//Blockly.Msg.GPS_Module_HELPURL = 'http://greich.fr'


  /*init: function() {
    this.appendField("Track user's Location")
        .appendField(new Blockly.FieldImage(Blockly.pathToBlockly + 'blocks/GPS_Module/GPS_Module.png', Blockly.Arduino.imageSize, Blockly.Arduino.imageSize));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.appendStatementInput("Date")
        .setCheck(null)
        this.appendStatementInput("Time")
        .setCheck(null)
        this.appendStatementInput("Speed")
        .setCheck('double')
        this.appendStatementInput("Retrieve_LAT_LNG")
        .setCheck(null)
        
    this.setColour("#20B2AA");
 this.setTooltip("This Block provides a precise, sensitive, and low-power GPS module for the user's position recognition and retrieves the longitude and latitude");
 this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
  }
};*/

/*Blockly.Blocks['GPS_Module'] = {
  init: function() {
    this.appendValueInput("Track_Location")
        .setCheck(null)
        .appendField("Track User's Location     ")
        .appendField(new Blockly.FieldImage(Blockly.pathToBlockly + 'blocks/GPS_Module/GPS_Module.png', Blockly.Arduino.imageSize, Blockly.Arduino.imageSize));
    this.appendStatementInput("Date")
        .setCheck(null);
    this.appendStatementInput("Time")
        .setCheck(null);
    this.appendStatementInput("Speed")
        .setCheck(null);
    this.appendStatementInput("Retrieve_LAT_LNG")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#20B2AA");
    this.setTooltip("This Block provides a precise, sensitive, and low-power GPS module for the user's position recognition and retrieves the longitude and latitude");
    this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
  }
};*/

Blockly.Blocks['GPS_Module'] = {
  init: function() {
    this.appendDummyInput("")
        .appendField("Retrieve Longtitude and Latitude")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#FFD700");
    this.setTooltip("This Block retrieves the longitude and latitude using GPS Module");
    this.setHelpUrl("https://wiki.seeedstudio.com/Grove-GPS/");
  }
};

