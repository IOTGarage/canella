/**
 * Visual Blocks for Measure Heart Rate Blocks
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

'use strict';

goog.provide('Blockly.Arduino.Heart_Rate'); 
goog.require('Blockly.Arduino');

Blockly.Arduino['Heart_Rate'] = function() {
    // TODO: Assemble JavaScript into code variable.

  var value_HeartRate= Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
  var port = Blockly.Arduino.valueToCode(this, 'PIN', Blockly.Arduino.ORDER_ATOMIC).replace(/['"]+/g, '');
  //var value_HeartRat = block.getFieldValue('Name');
Blockly.Arduino.includes_["includes_Wire_lib"] = '#include <Wire.h>\n' + 'unsigned char c; \n' + ' int total = 0; \n' + 'unsigned char HeartRateArray[10] = {}; \n' + 'int i = 0;  \n' +  'String Heart_Rate = "0"; \n'

                            
Blockly.Arduino.setups_["setup_Heart_Rate"] = 'Wire.begin();'

var code = "";
var code =    

' Wire.requestFrom(0x' + port + ' >> 1, 1);    // request 1 bytes from slave device \n' +
' while(Wire.available()) \n' +
' { \n' +
" "+value_HeartRate +" \n" +
' } // end while statement for reading Heart Rate data \n'
return code;
};

Blockly.Arduino['Measure_Heart_Rate'] = function(block) {

    var Value_HeartRate= Blockly.Arduino.valueToCode(block, 'Measure_Heart_Rate', Blockly.Arduino.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.

var code = "";
var code =    

' c = Wire.read();   // receive heart rate value (a byte) \n' +
' total = total + c; \n' +
' HeartRateArray[count] = c; //Add heart rate values to an array to retrieve them after a workout is completed \n' +
' Heart_Rate  = Heart_Rate + ","+ String(HeartRateArray[count]); //Add heart rate data in a list of String \n' +
'//EEPROM.write(3, Heart_Rate); //Store the heart rate values on the Arduino board \n'+
" "+Value_HeartRate+" \n" 
return code;
};


Blockly.Arduino['Measure_Active_Calories'] = function(block) {
    // TODO: Assemble JavaScript into code variable.

    var Value_Calories= Blockly.Arduino.valueToCode(block, 'Measure_Active_Calories', Blockly.Arduino.ORDER_ATOMIC);
    Blockly.Arduino.definitions_["define_Measure_calories"] = 'int Age= 31;\n' + ' float Weight= 58;\n' + 'int Time = 5;\n' + 'String sex;\n' + 'float Calories_Burned;\n' + 'float Total_calories; \n' + 'unsigned int ActiveCaloriesArray [10] = {};\n' + 'String Calories;\n'


var value_HeartRate = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);


var code = "";
var code = 'Calories_Burned = (-55.0969+((.6309*c)+(0.1988*58)+(0.2017*32)))/(4.184*60*(millis()/ 1000)); \n' +
'Total_calories = Total_calories + Calories_Burned; \n' +
'Calories=  String(Total_calories); \n' +
'//EEPROM.write(5, Calories); //Store the calories on the Arduino board \n'+
" "+Value_Calories+" \n" 
return code;
};


Blockly.Arduino['Print_Heart_Rate_Minitue'] = function() {
    // TODO: Assemble JavaScript into code variable.


//var value_print = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = "" 
var code = ' Serial.print("Heart Rate per Minute:"); \n' +
' Serial.println(c, DEC); \n' +
' delay(500); \n' 

return code;
};

Blockly.Arduino['Print_Lists_Heart_Rate_Minitue'] = function() {
    // TODO: Assemble JavaScript into code variable.


var value_print = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = "" 
var code = ' Serial.print("Readings of Heart Rate per Minute:"); \n' +
'  Serial.println(Heart_Rate); \n'

return code;
};

Blockly.Arduino['Print_Active_Calories'] = function() {
    // TODO: Assemble JavaScript into code variable.


var value_print = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = "" 
var code = 'Serial.print("Active Calories: "); Serial.println(Total_calories);\n' +
' delay(500);\n' 

return code;
};

Blockly.Arduino['Print_Total_Calories'] = function() {
    // TODO: Assemble JavaScript into code variable.


var value_print = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = "" 
var code = '//Total Calories \n' +
'Serial.print("Total Calories:"); Serial.println(Calories); \n' 

return code;
};
