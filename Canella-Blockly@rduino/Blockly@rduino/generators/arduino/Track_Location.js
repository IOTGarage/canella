/**
 * Visual Blocks for Track Location Blocks
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */'use strict';


 


goog.provide('Blockly.Arduino.Track_Location'); 
goog.require('Blockly.Arduino');


Blockly.Arduino['Track_Location'] = function(block) {
    // TODO: Assemble JavaScript into code variable.
   var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
   var RX = Blockly.Arduino.valueToCode(this, 'PIN2', Blockly.Arduino.ORDER_ATOMIC);
   var TX = Blockly.Arduino.valueToCode(this, 'PIN3', Blockly.Arduino.ORDER_ATOMIC);
  

Blockly.Arduino.includes_["includes_TinyGPS_lib"] = '#include "TinyGPS++.h"\n';
Blockly.Arduino.includes_["includes_SoftwareSerial_lib"] = '#include "SoftwareSerial.h"\n'+ '#include <EEPROM.h> \n';
;

Blockly.Arduino.definitions_["define_Track_Location"] = "TinyGPSPlus gps; //This is the GPS object that will pretty much do all the grunt work with the NMEA data \n" +   'float myArray[10] = {};\n' +  'int index = 0;\n' + 'const unsigned long WorkoutInterval = 30000UL;// specify interval for a particular workout \n' + "SoftwareSerial serial_connection(" + RX + ", " + TX + "); //RX=pin 2, TX=pin 3 \n" + "int count = 0;  \n" 
                            
Blockly.Arduino.setups_["setup_Track_Location"] = "Serial.begin(9600);\n"  + "serial_connection.begin(9600); //This is the GPS object that will pretty much do all the grunt work with the NMEA data \n" 


var code =
"  while(serial_connection.available()) //While there are characters to come from the GPS \n" +
" {\n" +
" gps.encode(serial_connection.read()); //This feeds the serial NMEA data into the library one char at a time \n" +
" }\n" +
" if(gps.location.isUpdated()) //This will pretty much be fired all the time anyway but will at least reduce it to only after a package of NMEA data comes in \n" +
"  {\n" +
" if (millis() <= WorkoutInterval) \n" +
" { \n" +
" "+value_location+" \n" +
' count++; // Count Number of reading of data \n' +
"} // end of Interval Time to read data from sensors \n" +
' } //end of a package of NMEA data coming  \n'+
"if (gps.charsProcessed() < 10) \n" +
' Serial.println(F("WARNING: No GPS data.  Check wiring.")); \n'

return code;
};




Blockly.Arduino['Date'] = function() {
    // TODO: Assemble JavaScript into code variable.

Blockly.Arduino.definitions_["define_Date"] = "int Day;\n" + "int Month;\n"+ "int Year;\n" 

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code =   '//Retrieve Date \n' +
'Day = gps.date.day(); // Day (1-31) (u8) \n' +
' Month = gps.date.month(); // Month (1-12) (u8) \n' +
' Year = gps.date.year(); // Year (2000+) (u16) \n' +
'String datevalue = String(Day) +"/"+ String(Month) +"/"+  String(Year);\n' +
'//EEPROM.write(0, datevalue); //Store date on the Arduino board \n'
return code;
};

Blockly.Arduino['Print_Date'] = function() {
    // TODO: Assemble JavaScript into code variable.

Blockly.Arduino.definitions_["define_Date"] = "int Day;\n" + "int Month;\n"+ "int Year;\n" 

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
var code =   '//Display Date \n' +
'Serial.print("Date:"); Serial.print(Day); Serial.print("/"); Serial.print(Month);Serial.print("/");Serial.println(Year); // Date \n' + 
' delay(500); \n' 
return code;
};

Blockly.Arduino['Current_Time'] = function() {
    // TODO: Assemble JavaScript into code variable.

Blockly.Arduino.definitions_["define_Time"] = "int Hour;\n" + "int Minute;\n"+ "int Second;\n" 

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code =  ' //Retrieve Time \n' + 
' Hour = gps.time.hour(); \n' + 
' Minute = gps.time.minute(); \n' + 
' Second = gps.time.second(); \n' +
'String timevalue = String(Hour) +"/"+ String(Minute) +"/"+  String(Second);\n' +
'//EEPROM.write(1, timevalue); //Store time on the Arduino board \n'
return code;
};


Blockly.Arduino['Print_Current_Time'] = function() {
    // TODO: Assemble JavaScript into code variable.


var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
var code =  ' //Display Time \n' + 
 ' Serial.println("Time:"+ String(Hour) +":" + String(Minute) +":"+ String(Second)); //Current Time \n' +
' delay(500); \n' 
return code;
};

Blockly.Arduino['Time'] = function(block) {
    // TODO: Assemble JavaScript into code variable.
    var Value_Time= Blockly.Arduino.valueToCode(block, 'Time', Blockly.Arduino.ORDER_ATOMIC);
Blockly.Arduino.definitions_["define_Time"] = "int Hour;\n" + "int Minute;\n"+ "int Second;\n" + 'String Spent_Time;\n'
var code =  ' //Retrieve Activity Spent Time \n' + 
'Second = millis()/ 1000;\n' + 
'Minute = Second/60;\n' + 
'Hour = Minute/60;\n' +
'String SpentTimeValue = String(Second) +"/"+ String(Minute) +"/"+  String(Hour);\n' +
'//EEPROM.write(2, SpentTimeValue); //Store Activity Spent Time on the Arduino board \n'+
" "+Value_Time+" \n" 
return code;
};


Blockly.Arduino['Print_Time'] = function() {
    // TODO: Assemble JavaScript into code variable.

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code =  ' //Display Activity Spent Time \n' + 
 ' Serial.println("Activity Spent Time:"+ String(Hour) +":" + String(Minute) +":"+ String(Second)); // Activity Spent Time \n' +
' delay(500); \n' 
return code;
};


Blockly.Arduino['LAT_LNG'] = function(block) {
    // TODO: Assemble JavaScript into code variable.
   var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
   //var Value_LATLNG = Blockly.Arduino.valueToCode(block, 'LAT_LNG', Blockly.Arduino.ORDER_ATOMIC);
   var Value_Print= Blockly.Arduino.valueToCode(block, 'LAT_LNG', Blockly.Arduino.ORDER_ATOMIC);
   

Blockly.Arduino.definitions_["define_LAT_LNG"] = "float LAT; //Define Latitude \n"  + "float LNG; //Define Longtitude \n"  +  "String LocationArray [10] = {}; \n" + 'String Location_Data= " "; \n' 
var code =
"//Retrieve Location \n" +
"LAT = gps.location.lat();//This will retrieve Latitude of a particular position \n" +
"LNG = gps.location.lng(); //This will retrieve Longtitude of a particular position\n" +
"String latitude = String(LAT,6); \n" +
"String longitude = String(LNG,6); \n" +
' LocationArray[count] = latitude + " " + longitude; //Add Latitudes and Longtitudes to an array to retrieve them after a workout is completed \n' +
" "+Value_Print+" \n" +
' Location_Data = LocationArray[count] + "," + Location_Data; //Add Latitudes and Longtitudes in a list of String\n'+
'//EEPROM.write(3, Location_Data); //Store Latitude and Longtitude on the Arduino board \n'

//" "+Value_LATLNG+" \n" 


return code;
};

Blockly.Arduino['Print_Location'] = function() {
    // TODO: Assemble JavaScript into code variable.

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
var code = "";
    
var code =  ' //Display Location \n' +
'Serial.print("Latitude and Longitude:"); Serial.print(latitude); Serial.print(","); Serial.println(longitude); \n' +
'delay(500); \n'

return code;
};



     
 

Blockly.Arduino['Print_Lists_Locations'] = function() {
    // TODO: Assemble JavaScript into code variable.

    var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
    var code = "";
        
    var code =  ' //Readings of Location Data for a workout\n' +
    ' Serial.print("Readings of Locations:"); Serial.println(Location_Data); \n'
    
    return code;
};


Blockly.Arduino['Speed'] = function(block) {
    // TODO: Assemble JavaScript into code variable.

    var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
    var Value_Speed= Blockly.Arduino.valueToCode(block, 'Speed', Blockly.Arduino.ORDER_ATOMIC);
    
    Blockly.Arduino.definitions_["define_Speed"] = 'double Speed; \n' + 'String Speed_Data="0.00";\n' + 'double SpeedArray[10] = {}; \n' + 'String Speed_Range;\n'


var code =  ' //Retrieve Speed \n' +
'Speed = gps.speed.mps(); // Speed in meters per second (double) \n' +
'Speed_Data = Speed_Data + "," + String(SpeedArray[count]); //Add speed values to an array to retrieve them after a workout is completed\n' +
' int Total_Speed = Total_Speed + Speed;\n' +
' '+Value_Speed+' \n'+
'//EEPROM.write(6, Speed_Data); //Store Speed Values on the Arduino board \n' 


return code;
};



Blockly.Arduino['Print_Speed'] = function() {
    // TODO: Assemble JavaScript into code variable.

var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = ' //Display Speed \n' +
'Serial.print("Speed:");Serial.println(Speed);\n' +
"delay(500); \n" 
return code;
};


Blockly.Arduino['Print_Lists_Speed'] = function() {
        // TODO: Assemble JavaScript into code variable.

    var code = '//Readings of speed for a workout \n' +
    'Serial.print("Readings of Speed:"); Serial.println(Speed_Data);\n'
    return code;
   };
  
     
Blockly.Arduino['Distance'] = function(block) {
    // TODO: Assemble JavaScript into code variable.
    var value_location = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
    var Value_Distance= Blockly.Arduino.valueToCode(block, 'Distance', Blockly.Arduino.ORDER_ATOMIC);

    Blockly.Arduino.definitions_["define_Distance"] = 'double Distance; \n' + 'String Distance_Range;\n'



var code = 
" //Retrieve Distance \n" + 

" if (gps.location.isValid())  \n" +
"{ \n"+
  " static const double Home_LAT = 51.488956, Home_LON =-3.1793029;  \n" +
  " Distance =  \n"+
    " TinyGPSPlus::distanceBetween( \n" +
      " gps.location.lat(),  \n" +
      "  gps.location.lng(),  \n" +
      "  myArray[0],  \n" +
      "  myArray[1]);  \n" +
      "  } \n" +
      " "+Value_Distance+" \n" +
      " //EEPROM.write(7, Distance); //Store the distance on the Arduino board \n" 


return code;
};


Blockly.Arduino['Print_Distance'] = function() {
    // TODO: Assemble JavaScript into code variable.

var value_Speed = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = ' //Display Distance \n' + 
  ' Serial.print("Distance:");  \n' +
  ' Serial.println(Distance/1000, 2);  \n' +
' delay(500);  \n'

return code;
};

Blockly.Arduino['Print_Total_Distance'] = function() {
    // TODO: Assemble JavaScript into code variable.

var value_Speed = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);

var code = ' //Display Total Distance for a workout\n' + 
  ' Serial.print("Total Distance:");  \n' +
  ' Serial.println(Distance/1000, 2);  \n'

return code;
};

