'use strict';

goog.provide('Blockly.Arduino.Minimise_Raw'); 
goog.require('Blockly.Arduino');

Blockly.Arduino.Minimise_Raw = function(block) {
    
    
var data_type = block.getFieldValue('Minimise_Raw');
var value_name = block.getFieldValue('Time');
var value_num =  block.getFieldValue('Value');

Blockly.Arduino.includes_["includes_Minimise_Raw_Def"] = 'int total_Avergae_HeartRate= 0; \n' + 'int count_Avergae_HeartRate= 0; \n' + 'int x_Avergae_SpecificTime= 0;\n' +  'unsigned long currentMillis = millis();\n' +
' unsigned long period = '+value_num+'; \n' +
' unsigned char AverageHeartRateArray['+value_num+'] = {}; \n' +
' int average_per_time = 0; \n' +
' String AverageHeartRateArrayList= "0"; \n' +
' int Total_Speed = 0; \n' +
' int count_Speed = 0;  \n' +
' unsigned char AverageSpeedArray['+value_num+'] = {};\n' +
' int average_speed_per_time = 0; \n' +
' String AverageSpeedArrayList = "0";\n' 


if (data_type == "Heart_Rate")
{
  
  if (value_name == "Seconds") // if Seconds is selected Avergae data based on the specified value
{
// TODO: Assemble JavaScript into code variable.

var code = "";
var code = 

' //Calculate Average Heart Rate for a specified period during a particular workout \n' +

' total_Avergae_HeartRate = total_Avergae_HeartRate+c; //sum specific heart rate values to calculate the average heart rate per a particular time \n' +
' count_Avergae_HeartRate++;// Count Number of Heart Rate readings to calculate average heart rate per a specific time \n' +

 ' if ((currentMillis/1000) == period) //To average heart rate during a particular workout every specified time \n' +
   ' { \n' +
    ' average_per_time = total_Avergae_HeartRate/count_Avergae_HeartRate; \n' +
    'period = period+ '+value_num+'; \n' +
    'total_Avergae_HeartRate=0; \n' +
    'count_Avergae_HeartRate=0; \n' +
    'AverageHeartRateArray[x_Avergae_SpecificTime] = average_per_time; \n' +
    'x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n' +
    '} \n' +
  
    'if (millis() >  WorkoutInterval) //To print the average heart rate for a specified period during a particular workout \n' +
    '{\n' +
    'for (int y= 0; y <= sizeof(period); y++) \n' +
    '{\n' +
    'AverageHeartRateArrayList=  AverageHeartRateArrayList + "," + String(AverageHeartRateArray[y]);\n' +
    '}\n' +
    'Heart_Rate = "Average heart rate for 5s period is "+ AverageHeartRateArrayList;\n'+ 
'}\n' 

}


else if  (value_name == "Minutes") // if Minutes is selected Avergae data based on the specified value
{

var code = "";
var code = 
  ' //Calculate Average Heart Rate for a specified period during a particular workout \n' +

  ' total_Avergae_HeartRate = total_Avergae_HeartRate+c; //sum specific heart rate values to calculate the average heart rate per a particular time \n' +
' count_Avergae_HeartRate++;// Count Number of Heart Rate readings to calculate average heart rate per a specific time \n' +

   ' if (((currentMillis/1000)/60) == period)  //To average heart rate during a particular workout every specified time \n' +
     ' { \n' +
      ' average_per_time = total_Avergae_HeartRate/count_Avergae_HeartRate; \n' +
      'period = period+ '+value_num+'; \n' +
      'total_Avergae_HeartRate=0; \n' +
      'count_Avergae_HeartRate=0; \n' +
      'AverageHeartRateArray[x_Avergae_SpecificTime] = average_per_time; \n' +
      ' x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n' +
      '} \n' +
    
      'if (millis() >  WorkoutInterval) //To print the average heart rate for a specified period during a particular workout \n' +
      '{\n' +
      'for (int y= 0; y <= sizeof(period); y++) \n' +
      '{\n' +
      'AverageHeartRateArrayList=  AverageHeartRateArrayList + "," + String(AverageHeartRateArray[y]);\n' +
      '}\n' +
      'Heart_Rate = "Average heart rate for 5s period is "+ AverageHeartRateArrayList;\n'+ 
  '}\n' 
    
}


else if  (value_name == "Hours") // if Hours is selected Avergae data based on the specified value
{

  var code = "";
  var code = 

  ' //Calculate Average Heart Rate for a specified period during a particular workout \n' +
  ' total_Avergae_HeartRate = total_Avergae_HeartRate+c; //sum specific heart rate values to calculate the average heart rate per a particular time \n' +
' count_Avergae_HeartRate++;// Count Number of Heart Rate readings to calculate average heart rate per a specific time \n' +

   ' if (((((currentMillis/1000)/60))/60) == period)  //To average heart rate during a particular workout every specified time \n' +
     ' { \n' +
      ' average_per_time = total_Avergae_HeartRate/count_Avergae_HeartRate; \n' +
      'period = period+ '+value_num+'; \n' +
      'total_Avergae_HeartRate=0; \n' +
      'count_Avergae_HeartRate=0; \n' +
      'AverageHeartRateArray[x_Avergae_SpecificTime] = average_per_time; \n' +
      ' x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n' +
      '} \n' +
    
      'if (millis() >  WorkoutInterval) //To print the average heart rate for a specified period during a particular workout \n' +
      '{\n' +
      'for (int y= 0; y <= sizeof(period); y++) \n' +
      '{\n' +
      'AverageHeartRateArrayList=  AverageHeartRateArrayList + "," + String(AverageHeartRateArray[y]);\n' +
      '}\n' +
      'Heart_Rate = "Average heart rate for 5s period is "+ AverageHeartRateArrayList;\n'+ 
  '}\n' 
}

}


else if (data_type == "Speed")
{
  if (value_name == "Seconds") 
{
 

 var code = "";
 var code = 
' //Calculate Average Speed for a specified period during a particular workout \n' +
 ' Total_Speed = Total_Speed + Speed; \n' +
 ' count_Speed++; \n' +
 ' if ((currentMillis/1000) == period) //To average speed during a particular work out every specified time \n' +
 ' { \n' +
 ' average_speed_per_time = Total_Speed/count_Speed; \n' +
 'period = period+ '+value_num+'; \n' +
 ' Total_Speed=0; \n' +
 ' count_Speed=0; \n' +
 ' AverageSpeedArray[x_Avergae_SpecificTime] = average_speed_per_time; \n' +
 ' x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n'  +
 ' } \n' +

 ' if (millis() >  WorkoutInterval) //To aggregate speed  during a particular work out \n' +
 '{\n' +
  ' for (int y= 0; y <= sizeof(period); y++) \n' +
  ' { \n' +
  '  AverageSpeedArrayList=  AverageSpeedArrayList + "," + String(AverageSpeedArray[y]); \n' +
  ' } \n' +

  'Speed_Data = "Average speed for 5s period is "+ AverageSpeedArrayList;\n'+ 
  
  ' } \n' 

}

if (value_name == "Minutes")
{
  var code = "";
  var code = 
  ' //Calculate Average Speed for a specified period during a particular workout \n' +
  ' Total_Speed = Total_Speed + Speed; \n' +
  ' count_Speed++; \n' +
  ' if (((currentMillis/1000)/60) == period) //To average speed during a particular work out every specified time \n' +
  ' { \n' +
  ' average_speed_per_time = Total_Speed/count_Speed; \n' +
  'period = period+ '+value_num+'; \n' +
  ' Total_Speed=0; \n' +
  ' count_Speed=0; \n' +
  ' AverageSpeedArray[x_Avergae_SpecificTime] = average_speed_per_time; \n' +
  ' x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n'  +
  ' } \n' +

  ' if (millis() >  WorkoutInterval) //To aggregate speed  during a particular work out \n' +
  '{\n' +
   ' for (int y= 0; y <= sizeof(period); y++) \n' +
   ' { \n' +
   '  AverageSpeedArrayList=  AverageSpeedArrayList + "," + String(AverageSpeedArray[y]); \n' +
   ' } \n' +
   'Speed_Data = "Average speed for 5s period is "+ AverageSpeedArrayList;\n'+ 
   ' } \n' 
}

if (value_name == "Hours")
{
  var code = "";
  var code = 
  ' //Calculate Average Speed for a specified period during a particular workout \n' +
  ' Total_Speed = Total_Speed + Speed; \n' +
  ' count_Speed++; \n' +
  '  if (((((currentMillis/1000)/60))/60) == period)  //To average speed during a particular work out every specified time \n' +
  ' { \n' +
  ' average_speed_per_time = Total_Speed/count_Speed; \n' +
  'period = period+ '+value_num+'; \n' +
  ' Total_Speed=0; \n' +
  ' count_Speed=0; \n' +
  ' AverageSpeedArray[x_Avergae_SpecificTime] = average_speed_per_time; \n' +
  ' x_Avergae_SpecificTime = x_Avergae_SpecificTime+1; \n'  +
' }\n' +
  ' if (millis() >  WorkoutInterval) //To aggregate speed  during a particular work out \n' +
  '{\n' +
   ' for (int y= 0; y <= sizeof(period); y++) \n' +
   ' { \n' +
   '  AverageSpeedArrayList=  AverageSpeedArrayList + "," + String(AverageSpeedArray[y]); \n' +
   ' } \n' +
 
   'Speed_Data = "Average speed for 5s period is "+ AverageSpeedArrayList;\n'+ 
   ' } \n' 
}
}
return [code, Blockly.Arduino.ORDER_ATOMIC];
};