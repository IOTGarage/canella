/**
 * Visual Blocks for Retrieving Workout Data
 *
 * Copyright 2022 Atheer Aljeraisy.
 *
 */

 'use strict';

 goog.provide('Blockly.Arduino.Retrieve_Data'); 
 goog.require('Blockly.Arduino');
 
 Blockly.Arduino['Retrieve_Data'] = function() {
     // TODO: Assemble JavaScript into code variable.
 
     var value_Retrieve_Data = Blockly.Arduino.statementToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
 
 //Blockly.Arduino.includes_["includes_Wire_lib"] = '#include <Wire.h>\n' + 'unsigned char c; \n' + 'int count = 0; \n' + ' int total = 0; \n' + 'unsigned char HearRateArray [100] = {}; \n' + ' unsigned int ActiveCaloriesArray [10] = {}; \n' + 'int i = 0;  \n' +  'String Heart_Rate = ""; \n'
 
                             
 //Blockly.Arduino.setups_["setup_Retrieve_Data"] = 'Wire.begin();'
 
 var code = "";
 
 var code =    
 '   if (millis() > WorkoutInterval) //To retrieve workout data \n' +
 ' { \n' +

 " "+value_Retrieve_Data +" \n" +
 ' } // end time for a particular workout\n' 
 return code;
 };