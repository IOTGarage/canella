'use strict';

goog.provide('Blockly.Arduino.Anonymization');
goog.require('Blockly.Arduino');

Blockly.Arduino['Anonymization'] = function(block) {
  var dataType = block.getFieldValue('Data_Anonymization');
  var anonymizationType = block.getFieldValue(dataType + '_Value');

  var code = 'String result = Age;\n'; // Default result to original Age

  switch (anonymizationType) {
    case 'Pseudonymization':
      code += `
String pseudonymize(String dataType) {
  String firstNames[] = {"Alex", "Jordan", "Taylor", "Morgan", "Casey"};
  String lastNames[] = {"Smith", "Johnson", "Williams", "Jones", "Brown"};
  if (dataType == "First Name") {
    return firstNames[random(5)];
  } else if (dataType == "Last Name") {
    return lastNames[random(5)];
  } else {
    return "Pseudonymized";
  }
}

result = pseudonymize("${dataType}");
`;
      break;
    case 'Data Masking':
      code += `
String maskData(String dataType, String data) {
  if (dataType == "First Name" || dataType == "Last Name") {
    if (data.length() > 1) {
      return data[0] + String('*').repeat(data.length() - 1);
    } else {
      return "*";
    }
  } else if (dataType == "Telephone Number") {
    return data.substring(0, data.length() - 4).replaceAll("[0-9]", "*") + data.substring(data.length() - 4);
  }
  return data;
}

result = maskData("${dataType}", Age);
`;
      break;
    case 'Randomization':
      code += `
String randomizeData(String dataType, String data) {
  if (dataType == "Age") {
    int age = data.toInt();
    int randomOffset = random(-2, 3);
    return String(age + randomOffset);
  } else if (dataType == "Height") {
    float height = data.toFloat();
    float randomOffset = (random(-200, 201) / 100.0);
    return String(height + randomOffset);
  } else if (dataType == "Weight") {
    float weight = data.toFloat();
    float randomOffset = (random(-200, 201) / 100.0);
    return String(weight + randomOffset);
  } else if (dataType == "Telephone Number") {
    for (int i = 0; i < data.length(); i++) {
      if (isdigit(data[i])) {
        data[i] = '0' + random(10);
      }
    }
    return data;
  } else if (dataType == "Date" || dataType == "Date of Birth") {
    String dateParts[] = data.split("-");
    int year = dateParts[0].toInt();
    int month = dateParts[1].toInt();
    int day = dateParts[2].toInt();
    int randomOffset = random(-30, 31);
    day += randomOffset;
    // Adjust the date here, ensuring the date is valid
    return String(year) + "-" + String(month) + "-" + String(day);
  } else if (dataType == "Current Time") {
    String timeParts[] = data.split(":");
    int hours = timeParts[0].toInt();
    int minutes = timeParts[1].toInt();
    int seconds = timeParts[2].toInt();
    int randomOffset = random(-3600, 3601); // random offset in seconds
    seconds += randomOffset;
    // Adjust the time here, ensuring the time is valid
    return String(hours) + ":" + String(minutes) + ":" + String(seconds);
  } else if (dataType == "Distance" || dataType == "Burned Calories") {
    float value = data.toFloat();
    float randomOffset = (random(-50, 51) / 10.0);
    return String(value + randomOffset);
  } else if (dataType == "Medical Records") {
    String records[] = data.split(",");
    for (int i = 0; i < records.length(); i++) {
      float value = records[i].toFloat();
      float randomOffset = (random(-50, 51) / 10.0);
      records[i] = String(value + randomOffset);
    }
    return records.join(",");
  }
  return data;
}

result = randomizeData("${dataType}", Age);
`;
      break;
    case 'Generalization':
      code += `
String generalizeData(String dataType, String data) {
  if (dataType == "First Name" || dataType == "Last Name") {
    return data[0] + ".";
  } else if (dataType == "Gender") {
    return data.substring(0, 1) + ".";
  } else if (dataType == "Disability") {
    return "D";
  } else if (dataType == "Telephone Number") {
    return data.replace(/(\\d{3})(\\d{3})(\\d{4})/, "XXX-XXX-$3");
  }
  return "Generalized";
}

result = generalizeData("${dataType}", Age);
`;
      break;
    case 'Categorization':
      code += `
String categorizeData(String dataType, String data) {
  if (dataType == "Age") {
    int age = data.toInt();
    if (age < 18) return "Child";
    else if (age < 65) return "Adult";
    else return "Senior";
  } else if (dataType == "Height") {
    int height = data.toInt();
    if (height < 150) return "Short";
    else if (height < 180) return "Average";
    else return "Tall";
  } else if (dataType == "Weight") {
    int weight = data.toInt();
    if (weight < 50) return "Underweight";
    else if (weight < 80) return "Normal weight";
    else return "Overweight";
  } else if (dataType == "Current Time") {
    String timeParts[] = data.split(":");
    int hours = timeParts[0].toInt();
    if (hours < 12) return "Morning";
    else if (hours < 18) return "Afternoon";
    else return "Evening";
  }
  return "Categorized";
}

result = categorizeData("${dataType}", Age);
`;
      break;
    case 'Suppression':
      code += `
String suppressData(String dataType) {
  return "";
}

result = suppressData("${dataType}");
`;
      break;
    case 'Differential Privacy':
      code += `
String applyDifferentialPrivacy(String dataType, String data) {
  float value = data.toFloat();
  float epsilon = 1.0;
  float sensitivity = 1.0;
  float scale = sensitivity / epsilon;
  float noise = scale * log(1 - 2 * abs(random(1000) / 1000.0 - 0.5));
  return String(value + noise);
}

result = applyDifferentialPrivacy("${dataType}", Age);
`;
      break;
    default:
      code = 'result = Age;';
      break;
  }

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
