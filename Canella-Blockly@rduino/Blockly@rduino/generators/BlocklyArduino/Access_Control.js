'use strict';

goog.provide('Blockly.Arduino.Access_Control');
goog.require('Blockly.Arduino');

Blockly.Arduino['Data_Access_Control'] = function(block) {
  var statements = Blockly.Arduino.statementToCode(block, 'Name');
  var code = statements;
  return code;
};

Blockly.Arduino['authenticate_user'] = function(block) {
  var userType = block.getFieldValue('User_Types');

  var passwordTypes = [];
  if (block.getFieldValue('Password') === 'TRUE') {
    passwordTypes.push('Alphanumeric Password');
  }
  if (block.getFieldValue('BiometricPassword') === 'TRUE') {
    passwordTypes.push('Biometric Password');
  }
  if (block.getFieldValue('TokenBasedPassword') === 'TRUE') {
    passwordTypes.push('Token-based Password');
  }
  if (block.getFieldValue('IDPassword') === 'TRUE') {
    passwordTypes.push('ID-based Password');
  }
  if (block.getFieldValue('FaceRecognitionPassword') === 'TRUE') {
    passwordTypes.push('Face Recognition Password');
  }
  if (block.getFieldValue('FingerprintPassword') === 'TRUE') {
    passwordTypes.push('Fingerprint Password');
  }

  var code = 'accessControl({\n';
  code += '  userTypes: ["' + userType + '"],\n';
  code += '  passwordTypes: ["' + passwordTypes.join('", "') + '"]\n';
  code += '});\n';
  return code;
};

Blockly.Arduino['authorize_user'] = function(block) {
  var permissions = [];
  if (block.getFieldValue('Read') === 'TRUE') {
    permissions.push('Read');
  }
  if (block.getFieldValue('Write') === 'TRUE') {
    permissions.push('Write');
  }
  if (block.getFieldValue('Delete') === 'TRUE') {
    permissions.push('Delete');
  }
  if (block.getFieldValue('Download') === 'TRUE') {
    permissions.push('Download');
  }

  var code = 'accessControl({\n';
  code += '  permissions: ["' + permissions.join('", "') + '"]\n';
  code += '});\n';
  return code;
};
