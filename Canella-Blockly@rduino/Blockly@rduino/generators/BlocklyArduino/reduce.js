'use strict';

goog.provide('Blockly.Blocks.reduce');

goog.require('Blockly.Arduino');

Blockly.Arduino['reduce'] = function(block) {
    var dropdown_location_granularity = block.getFieldValue('Location_Granularity');
    var apiKey = block.getFieldValue('FIELDNAME');
    
    Blockly.Arduino.definitions_["define_reduce"] = 'const String API_KEY = "' + apiKey + '";\n' +
    'const char* host = "maps.googleapis.com";\n' +
    'String Reduced_Location;\n' +
    'unsigned long lastConnectionTime = 0; // last time you connected to the server, in milliseconds\n' +
    'const unsigned long postingInterval = 60 * 1000; // delay between updates, in milliseconds\n' +
    '#include <ESP8266WiFi.h>\n' +
    '#include <ESP8266HTTPClient.h>\n';

    var code = '';
    code += 'if (millis() - lastConnectionTime > postingInterval) {\n' +
    '  lastConnectionTime = millis();\n' +
    '  if (WiFi.status() == WL_CONNECTED) { // Check if we are connected to the WiFi\n' +
    '    HTTPClient http;\n' +
    '    String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + String(latitude) + "," + String(longitude) + "&key=" + API_KEY;\n' +
    '    http.begin(url); //Specify the URL\n' +
    '    int httpCode = http.GET(); //Make the request\n' +
    '    if (httpCode > 0) { //Check for the returning code\n' +
    '      String payload = http.getString();\n' +
    '      Serial.println(payload);\n' +
    '      DynamicJsonBuffer jsonBuffer;\n' +
    '      JsonObject& root = jsonBuffer.parseObject(payload);\n' +
    '      if (!root.success()) {\n' +
    '        Serial.println("parseObject() failed");\n' +
    '        return;\n' +
    '      }\n' +
    '      JsonArray& results = root["results"];\n' +
    '      JsonObject& result = results[0];\n' +
    '      JsonArray& address_components = result["address_components"];\n' +
    '      for (JsonObject& address_component : address_components) {\n' +
    '        JsonArray& types = address_component["types"];\n';

    if (dropdown_location_granularity == "Post_Code") {
        code += '        if (types.contains("postal_code")) {\n' +
        '          Reduced_Location = address_component["long_name"].as<String>();\n' +
        '          Serial.println("Reduced Location (Post Code): " + Reduced_Location);\n' +
        '        }\n';
    } else if (dropdown_location_granularity == "City_Name") {
        code += '        if (types.contains("administrative_area_level_2")) {\n' +
        '          Reduced_Location = address_component["long_name"].as<String>();\n' +
        '          Serial.println("Reduced Location (City Name): " + Reduced_Location);\n' +
        '        }\n';
    } else if (dropdown_location_granularity == "Country_Name") {
        code += '        if (types.contains("country")) {\n' +
        '          Reduced_Location = address_component["long_name"].as<String>();\n' +
        '          Serial.println("Reduced Location (Country Name): " + Reduced_Location);\n' +
        '        }\n';
    }

    code += '      }\n' +
    '    } else {\n' +
    '      Serial.println("Error on HTTP request");\n' +
    '    }\n' +
    '    http.end(); //Free the resources\n' +
    '  }\n' +
    '}\n';

    return [code, Blockly.Arduino.ORDER_ATOMIC];
};
