
  'use strict';
  

  goog.provide('Blockly.Blocks.encrypt_and_send_data');
  
  goog.require('Blockly.Arduino');
  
  Blockly.Arduino['encrypt_and_send_data'] = function(block) {
    var protocol = block.getFieldValue('PROTOCOL');
    var url = Blockly.Arduino.valueToCode(block, 'URL', Blockly.Arduino.ORDER_ATOMIC);
  
    // Ensure the URL is properly defined
    if (!url) {
      url = '""';  // Default to an empty string if no URL is provided
    }
  
    // Include necessary libraries and definitions
    Blockly.Arduino.definitions_['include_ESP8266WiFi'] = '#include <ESP8266WiFi.h>';
    Blockly.Arduino.definitions_['include_WiFiClientSecure'] = '#include <WiFiClientSecure.h>';
    
    // Variable definitions
    Blockly.Arduino.definitions_['define_wifi_credentials'] = `
      const char* ssid = "your-SSID";
      const char* password = "your-PASSWORD";
      const char* host = ${url};
      const int httpsPort = 443;
      const char* fingerprint = "XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX";
      WiFiClientSecure client;
    `;
  
    // Setup code to connect to WiFi
    Blockly.Arduino.setups_['setup_wifi'] = `
      Serial.begin(115200);
      WiFi.begin(ssid, password);
  
      while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("Connecting to WiFi...");
      }
  
      Serial.println("Connected to WiFi");
      client.setFingerprint(fingerprint);
  
      if (!client.connect(host, httpsPort)) {
        Serial.println("Connection failed");
        return;
      }
    `;
  
    // Main loop code to send encrypted data
    var code = `
      String url = "/";
      String data = "";  // Assuming data is collected and stored in this variable
  
      client.print(String("POST ") + url + " HTTP/1.1\\r\\n" +
                   "Host: " + host + "\\r\\n" +
                   "Content-Type: application/json\\r\\n" +
                   "Content-Length: " + data.length() + "\\r\\n" +
                   "\\r\\n" + data);
  
      while (client.connected()) {
        String line = client.readStringUntil('\\n');
        if (line == "\\r") {
          break;
        }
      }
  
      String line = client.readStringUntil('\\n');
      Serial.println(line);
    `;
  
    return code;
  };
  