# Canella

<div align="center">
  <a href="https://www.youtube.com/watch?v=7V0q3MyAzCQ&t=75s" title="YouTube demo video">
    <img src="https://img.youtube.com/vi/7V0q3MyAzCQ/0.jpg" alt="YouTube demo video">
  </a>
</div>



## Description
Canella is an integrated IoT development ecosystem designed to prioritize privacy in IoT applications. It leverages End-User Development (EUD) tools such as Blockly@rduino and Node-RED to help developers create end-to-end IoT applications that comply with privacy regulations. Canella provides real-time feedback on privacy concerns, streamlines privacy implementation, and reduces cognitive load, thus enhancing productivity and privacy awareness among developers.

## Architecture of Canella

Canella's architecture aligns with IoT application architecture, where data moves from sensors to gateways and then to the cloud. The architecture encompasses IoT devices, gateways, and cloud platforms, each with diverse computational capabilities. Canella utilizes two widely used visual programming languages, Blockly@rduino and Node-RED, for creating privacy-preserving IoT applications.

![Canella's architecture](./images/Architecture1.jpg)

### Components
- **Blockly@rduino**: A web-based visual programming editor for Arduino, representing the edge of an IoT application.
- **Node-RED**: Represents the fog in Canella's IoT application, running on a Raspberry Pi for data processing and forwarding analytical summaries to the cloud.

## Design and Implementation

### Privacy-Preserving Components
Canella includes privacy blocks and nodes designed based on the Privacy by Design (PbD) guidelines to help you handle the privacy requirements of the application and comply with some of the CPLF principles, such as:
- **Reduce Location Granularity**: Converts GPS coordinates to human-readable addresses with selectable granularity.
- **Minimize Raw Data Intake**: Calculates averages over specified time frames to minimize raw data.
- **Category-based Aggregation**: Aggregates detailed data into predefined categories.
- **Minimize Data Storage and Retention Period**:Reduces the amount of stored data by updating or deleting raw data once secondary contexts are derived.
- **Access Control**: Enforces access control mechanisms based on specified criteria.
- **Data Anonymization**: Suggests different data anonymization techniques for anonymizing personally identifiable information before the data gets used by IoT applications.
- **Encrypted Data Storage**: Suggests encryption techniques based on type.
- **Encrypted Data Communication**: Encrypts and sends data using a specified protocol.

### Encouraging Privacy-Preserving Practices
- **Real-Time Feedback**: Provides developers with warnings about potential privacy issues and suggests privacy-preserving components.
- **Privacy Law Validator**: Checks data practices and provide visual feedback status for compliance with privacy and data protection laws based on the combined privacy law framework (CPLF) [here](https://dl.acm.org/doi/abs/10.1145/3450965).


## Usage

###  Blockly@rduino:

- To install it locally. Download Canella-Blockly@rduino and open ../Blockly@rduino/index.html

####  Set Up Your Environment:

- Ensure that you have the Arduino IDE installed on your computer. You can download it [here](https://www.arduino.cc/en/software).
- Connect your Arduino board to your computer.
- Use the Blockly@rduino interface to create your Arduino program using visual blocks.
- In Blockly@rduino, you can find Privacy Blocks by selecting the Privacy Blocks toolbox under the Configure Block tab.
- Generate the Arduino code from your Blockly@rduino design.
- Configure the settings to match your Arduino board and connected sensors.
- Upload the generated code to your Arduino board using the Arduino IDE.



### Node-RED:

- To run the Node-RED locally, follow the steps [here](https://nodered.org/docs/getting-started/local).
- Download Canella-Node-RED folder.
#### Add Privacy Nodes to your Node-RED:
- Cd (path of the new created node) 
- Npm link 
- cd ~/.node-red 
- npm link  (name of the created node) e.g., node-red-contrib-reduce 
- Node-red 
- In Node-RED, you can find these Privacy Nodes in the Node-RED palette under the Privacy Nodes category.

### Examples

Example configurations and usage scenarios can be found in the documentation and the provided demo video [here](https://www.youtube.com/watch?v=7V0q3MyAzCQ&t=75s).

## Prototyping: Fitness Tracking IoT Application

A hypothetical use case scenario demonstrates how Canella helps integrate privacy-preserving components into the data flow of a Fitness Tracking IoT application.  It is a helpful IoT-based system that enables trainees to track their activities easily. Additionally, it assists trainers in managing their trainees, easily monitoring their progress, and encouraging them for their health, wellness, and safety.

## Simulating the Wristband of a Fitness Tracking IoT Application

### Required Hardware Components
- Arduino Uno Microcontroller Board
- ESP8266 Wi-Fi Module
- Grove - Base Shield for Arduino
- Grove - GPS Module Sensor
- Grove - Finger Heart Rate Sensor with Shell

### Setting Up Arduino

#### Assemble the Hardware:
1. Attach the Grove Base Shield to the Arduino Uno.
2. Connect the sensors to the Grove Base Shield:
   - GPS Module Sensor to the A3 port.
   - Finger Heart Rate Sensor with Shell to the A0 port.
3. Connect the ESP8266 Wi-Fi Module to the appropriate pins on the Arduino (typically using the RX and TX pins for serial communication).

#### Programming Wristband via Blockly@rduino:
- In Blockly@rduino, we have created custom blocks for the Fitness Tracking IoT application.
- Click on Configure Block on the left side of Blockly workspace, select Fitness Tracking IoT application category.
- Now, on the left side, you are going to see the Fitness Tracking IoT application toolbox, which lists all the blocks to build a Fitness Tracking IoT application.
- Set up your workspace to simulate the wristband of the Fitness Tracking IoT application.
- Integrate the approprite privacy-preserving blocks.
- Now, Blockly@rduino will generate the code.
- From the left side, click on the Source code.
- Copy the code from the workspace to Arduino IDE.
- Upload the code and open the serial monitor. You should be able to see user’s workout data.



### Send data to the Node-red: To achieve this, do the following:

###  Required Hardware Components:
-	Raspberry pi
-	SD Card with OS installed on it (We’ll work on the Raspberry pi OS)
-	Display and HDMI cable
-	Keyboard and mouse
-	Power supply

### Setting Up Raspberry pi:
1.	Start by plotting the SD card into the SD card slot. 
2.	Next plug-in peripherals into the USB ports and connect Raspberry Pi to the monitor using the necessary (i.e., micro HDMI - DVI) cable.
3.	Power up your Raspberry Pi. You should see the red LED light turn on. As the device is booting, you’ll see raspberries displayed on your monitor.
4.	After the OS configured connect to the eduroam/Wi-Fi network. 
5.	Update the Pi and reboot once.
6.	Check if Raspberry Pi shows the correct time and date. If not, set the correct date and time.
7.	Check if I2C is enabled by clicking on the Raspberry icon(top left corner  ). 
8.	Then from Preferences choose Raspberry Pi Configuration and then Interfaces. 
9.	The Raspberry Pi is already preconfigured to run Node-RED without any issues.
10.	 Run the below code to confirm that your Pi works fine: 
     sudo i2cdetect -y 1

#### Send Data from Arduino to Nod-RED:
Everything should be ready to start programming with Node-RED. 
1. Open the applications menu by clicking on the Raspberry icon in top left corner. 
2. Then from programming tools choose Node-RED.

#### Programming the Fitness Tracking Application with Node-RED:
- In Node-red, you are required to visualise data coming from the Arduino. 
- Once you visualise data, store data in Node-RED, and send it to the cloud.

## Contributing to Privacy Nodes and Privacy Blcoks
- Detailed insights into the Privacy Blocks are available at the <a href="https://gitlab.com/IOTGarage/privacy-blocks" title="Privacy Blocks Repository"> Privacy Blocks Repository. </a>
- Detailed insights into the Privacy Nodes are available at the <a href="https://gitlab.com/IOTGarage/privacy-nodes" title="Privacy Nodes Repository"> Privacy Nodes Repository. </a>



## Authors and acknowledgment

This project is developed and maintained by the Canella team. We thank all our contributors and supporters for their valuable input and assistance.


