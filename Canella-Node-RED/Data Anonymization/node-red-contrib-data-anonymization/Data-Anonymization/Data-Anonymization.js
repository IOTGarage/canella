module.exports = function(RED) {
  function DataAnonymizationNode(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    node.on('input', function(msg) {
      let data = msg.payload;
      let dataType = config.SpecifyTypeofData;
      let anonymizationType = config.DataAnonymization;

      try {
        switch (anonymizationType) {
          case 'Pseudonymization':
            msg.payload = pseudonymize(dataType, data);
            break;
          case 'Data Masking':
            msg.payload = maskData(dataType, data);
            break;
          case 'Randomization':
            msg.payload = randomizeData(dataType, data);
            break;
          case 'Categorization':
            msg.payload = categorizeData(dataType, data);
            break;
          case 'Generalization':
            msg.payload = generalizeData(dataType, data);
            break;
          case 'Suppression':
            msg.payload = suppressData(dataType, data);
            break;
          case 'Differential Privacy':
            msg.payload = applyDifferentialPrivacy(dataType, data);
            break;
          default:
            node.error("Unknown anonymization type: " + anonymizationType);
        }
        node.send(msg);
      } catch (err) {
        node.error("Error in data anonymization: " + err.message);
      }
    });

    function pseudonymize(dataType, data) {
      const firstNames = ["Alex", "Jordan", "Taylor", "Morgan", "Casey"];
      const lastNames = ["Smith", "Johnson", "Williams", "Jones", "Brown"];
      if (dataType === "First Name") {
        return firstNames[Math.floor(Math.random() * firstNames.length)];
      } else if (dataType === "Last Name") {
        return lastNames[Math.floor(Math.random() * lastNames.length)];
      } else {
        return "Pseudonymized";
      }
    }

    function maskData(dataType, data) {
      if (dataType === "First Name" || dataType === "Last Name") {
        if (typeof data === 'string' && data.length > 1) {
          return data[0] + "*".repeat(data.length - 1);
        } else if (typeof data === 'string' && data.length === 1) {
          return "*";
        } else {
          return "";
        }
      } else if (dataType === "Telephone Number") {
        return data.replace(/\d(?=\d{4})/g, "*"); // Mask all but the last 4 digits
      }
      return data; // Return the original data if not First Name or Last Name
    }

    function randomizeData(dataType, data) {
      const AGE_OFFSET_RANGE = 2;
      const HEIGHT_OFFSET_RANGE = 2; // cm
      const WEIGHT_OFFSET_RANGE = 2; // kg
      const TIME_OFFSET_RANGE = 60 * 60 * 1000; // 1 hour in milliseconds
      const DOB_OFFSET_RANGE = 30; // days
      const MEDICAL_OFFSET_RANGE = 5; // For medical records values

      if (dataType === "Age" && !isNaN(data)) {
        let age = parseInt(data);
        let randomOffset = Math.floor(Math.random() * (2 * AGE_OFFSET_RANGE + 1)) - AGE_OFFSET_RANGE;
        return age + randomOffset;
      } else if (dataType === "Height" && !isNaN(data)) {
        let height = parseFloat(data);
        let randomOffset = (Math.random() * (2 * HEIGHT_OFFSET_RANGE)) - HEIGHT_OFFSET_RANGE;
        return (height + randomOffset).toFixed(2);
      } else if (dataType === "Weight" && !isNaN(data)) {
        let weight = parseFloat(data);
        let randomOffset = (Math.random() * (2 * WEIGHT_OFFSET_RANGE)) - WEIGHT_OFFSET_RANGE;
        return (weight + randomOffset).toFixed(2);
      } else if (dataType === "Telephone Number") {
        return data.replace(/\d/g, function() {
          return Math.floor(Math.random() * 10);
        });
      } else if (dataType === "Date of Birth" || dataType === "Date") {
        let date = new Date(data);
        if (!isNaN(date.getTime())) {
          let randomOffset = Math.floor(Math.random() * (2 * DOB_OFFSET_RANGE + 1)) - DOB_OFFSET_RANGE;
          date.setDate(date.getDate() + randomOffset);
          return date.toISOString().split('T')[0];
        }
      } else if (dataType === "Current Time") {
        let timeParts = data.split(':');
        if (timeParts.length === 3) {
          let date = new Date();
          date.setHours(parseInt(timeParts[0]), parseInt(timeParts[1]), parseInt(timeParts[2]), 0);
          let randomOffset = Math.floor(Math.random() * (2 * TIME_OFFSET_RANGE + 1)) - TIME_OFFSET_RANGE;
          date.setTime(date.getTime() + randomOffset);
          return date.toTimeString().split(' ')[0];
        }
      } else if (dataType === "Medical Records") {
        let randomizedData = {};
        for (let key in data) {
          if (data.hasOwnProperty(key) && !isNaN(data[key])) {
            let value = parseFloat(data[key]);
            let randomOffset = (Math.random() * (2 * MEDICAL_OFFSET_RANGE)) - MEDICAL_OFFSET_RANGE;
            randomizedData[key] = (value + randomOffset).toFixed(2);
          } else {
            randomizedData[key] = data[key];
          }
        }
        return randomizedData;
      } else if (dataType === "Distance" || dataType === "Burned Calories") {
        let value = parseFloat(data);
        let randomOffset = (Math.random() * (2 * MEDICAL_OFFSET_RANGE)) - MEDICAL_OFFSET_RANGE;
        return (value + randomOffset).toFixed(2);
      }

      // Default randomization for any other data types
      return data.toString().split('').sort(function() { return 0.5 - Math.random() }).join('');
    }

    function categorizeData(dataType, data) {
      if (dataType === "Age" || dataType === "Date of Birth") {
        let age = parseInt(data);
        if (!isNaN(age)) {
          if (age < 18) return "Child";
          else if (age < 65) return "Adult";
          else return "Senior";
        }
      } else if (dataType === "Height") {
        let height = parseInt(data);
        if (!isNaN(height)) {
          if (height < 150) return "Short";
          else if (height < 180) return "Average";
          else return "Tall";
        }
      } else if (dataType === "Weight") {
        let weight = parseInt(data);
        if (!isNaN(weight)) {
          if (weight < 50) return "Underweight";
          else if (weight < 80) return "Normal weight";
          else return "Overweight";
        }
      } else if (dataType === "Current Time") {
        let timeParts = data.split(':');
        if (timeParts.length === 3) {
          let hours = parseInt(timeParts[0]);
          if (hours < 12) return "Morning";
          else if (hours < 18) return "Afternoon";
          else return "Evening";
        }
      }
      return data;
    }

    function generalizeData(dataType, data) {
      if (dataType === "First Name" || dataType === "Last Name") {
        return data.charAt(0) + ".";
      } else if (dataType === "Home Address") {
        return data.replace(/(\d{1,3}\s)\S+/, "$1*****");
      } else if (dataType === "Disability") {
        return data.toLowerCase() === "disable" || data.toLowerCase() === "disabled" ? "D" : "Not D";
      } else if (dataType === "Gender") {
        if (data.toLowerCase() === "male" || data.toLowerCase() === "m") {
          return "G1";
        } else if (data.toLowerCase() === "female" || data.toLowerCase() === "f") {
          return "G2";
        } else {
          return "G3";
        }
      } else if (dataType === "Telephone Number") {
        return data.replace(/(\d{3})(\d{3})(\d{4})/, "XXX-XXX-$3"); // Generalize to show only the last 4 digits
      }
      return "Generalized";
    }

    function suppressData(dataType, data) {
      return "";
    }

    function applyDifferentialPrivacy(dataType, data) {
      function laplaceMechanism(value, epsilon, sensitivity) {
        function laplaceNoise(scale) {
          const u = Math.random() - 0.5;
          return scale * Math.sign(u) * Math.log(1 - 2 * Math.abs(u));
        }
        const scale = sensitivity / epsilon;
        return value + laplaceNoise(scale);
      }

      if ((dataType === "Age" || dataType === "Height" || dataType === "Weight" || dataType === "Distance" || dataType === "Burned Calories") && !isNaN(data)) {
        let value = parseFloat(data);
        let epsilon = 1; // Privacy parameter
        let sensitivity = 1; // Sensitivity of the data
        return laplaceMechanism(value, epsilon, sensitivity).toFixed(2);
      } else if (dataType === "Date of Birth" || dataType === "Date") {
        let date = new Date(data);
        if (!isNaN(date.getTime())) { // Check if the date is valid
          let epsilon = 1; // Privacy parameter
          let sensitivity = 1; // Sensitivity of the date data
          let daysNoise = Math.round(laplaceMechanism(0, epsilon, sensitivity));
          date.setDate(date.getDate() + daysNoise);
          return date.toISOString().split('T')[0];
        } else {
          throw new Error("Invalid Date format");
        }
      } else if (dataType === "Current Time") {
        let timeParts = data.split(':');
        if (timeParts.length === 3) {
          let date = new Date();
          date.setHours(parseInt(timeParts[0]), parseInt(timeParts[1]), parseInt(timeParts[2]), 0);
          let epsilon = 1; // Privacy parameter
          let sensitivity = 1; // Sensitivity of the time data
          let timeNoise = Math.round(laplaceMechanism(0, epsilon, sensitivity) * 1000); // Noise in milliseconds
          date.setTime(date.getTime() + timeNoise);
          return date.toTimeString().split(' ')[0];
        } else {
          throw new Error("Invalid Time format");
        }
      } else if (dataType === "Medical Records") {
        let epsilon = 1; // Privacy parameter
        let sensitivity = 1; // Sensitivity of the medical data
        let noisyRecord = {};
        for (let key in data) {
          if (data.hasOwnProperty(key) && typeof data[key] === 'number') {
            noisyRecord[key] = laplaceMechanism(data[key], epsilon, sensitivity);
          } else {
            noisyRecord[key] = data[key];
          }
        }
        return noisyRecord;
      }
      return data;
    }
  }

  RED.nodes.registerType("data-anonymization", DataAnonymizationNode);
};
