module.exports = function(RED) {
    function AccessControlNode(config) {
        RED.nodes.createNode(this, config);
        let node = this;

        // Retrieve configuration settings
        this.SpecifyTyoeofData = config.SpecifyTyoeofData || "Data Subject";
        this.AccessControl = config.AccessControl || [];
        this.Permissions = config.Permissions || [];

        // Node input handler
        node.on('input', function(msg) {
            // Access control logic
            let authorized = true;

            // Check if msg.userType matches the specified data type
            if (msg.payload.userType !== node.SpecifyTyoeofData) {
                authorized = false;
            }

            // Ensure the password types match exactly
            if (!Array.isArray(msg.payload.passwordTypes) || node.AccessControl.length !== msg.payload.passwordTypes.length || !node.AccessControl.every(pt => msg.payload.passwordTypes.includes(pt))) {
                authorized = false;
            }

            // Ensure the permissions match exactly
            if (!Array.isArray(msg.payload.permissions) || node.Permissions.length !== msg.payload.permissions.length || !node.Permissions.every(p => msg.payload.permissions.includes(p))) {
                authorized = false;
            }

            // Pass or block the message based on authorization
            if (authorized) {
                node.send(msg); // Pass the message to the next node
            } else {
                node.error("Access Denied", msg);
            }
        });
    }

    // Register the node type
    RED.nodes.registerType("access-control", AccessControlNode);
};
