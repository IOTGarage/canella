

// to provide the module access to the Node-RED runtime API
module.exports = function(RED) {
    const request = require('request');
    
  
            // To reduce the granularity of location  from the  latitude and longitude coordinates of the location provided
         function ReducedLocation(LAT, LNG, KEY, callback) {
               
         
        // let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`;
          //let url = `https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=${KEY}`;
          let url = `https://maps.googleapis.com/maps/api/geocode/json?address=9+Llandough+St,+Cardiff+CF24+4AW,+UKA&key=${KEY}`;
          //let url = `https://maps.googleapis.com/maps/api/geocode/json?postal_code=CF11+0JW&key=${KEY}`;

         // let url = `http://google.com/maps/api/geocode/json?q=CF11+0JW&key=${KEY}`;

        // 9 Llandough St, Cardiff CF24 4AW, UK
          
  
         
                request({ url, json: true}, (error, {body}) => {
                  let LocationArray = body.results[0].address_components;
                  let PostalCode="";
                  let CityName="";
                  Array.prototype.forEach.call(LocationArray, part => {
                  console.log(part)
                  if (part.types.includes("postal_code")) 
                    {
                       PostalCode= part.long_name;
                      
                    }
                  else if (part.types.includes("administrative_area_level_2")) 
                  {
                     CityName= part.long_name;
                    
                  }

                  else if (part.types.includes("country")) 
                  {
                    CountryName= part.long_name;
                    
                  }
  
               });
                  
                   if (body.error) {
                      callback("Unable to find location.", undefined);
                  }
                  else {
                      callback(undefined, {
                              "FullAddress": body.results[0].formatted_address,
                              "PostCode": PostalCode,
                              "CityName": CityName,
                              "CountryName": CountryName
                            
                              
                      })
                  }
              })
          }
  


  //Reduce Granularity Node
  
              function ReduceGranularityNode(config) {
                RED.nodes.createNode(this,config);
                var node = this;
                this.on('input', function(msg) {
                  
                  
                 /* var LATLNG = msg.payload.split(",",2);
                  var LAT= LATLNG[0].substring(0);
                  var LNG= LATLNG[1].substring(0,LATLNG[1].length);*/

                  var Location = msg.payload.split(",",msg.payload.length);

                  for (let i = 0 ; i<Location.length; i++){
                    //Results = Results + parseInt(Average[i]);
  
                    var LATLNG = Location[i].split(" ",2);
                    var LAT= LATLNG[0].substring(0);
                    var LNG= LATLNG[1].substring(0,LATLNG[1].length);
                    //Total = Results/Average.length;
                  
                  
                  const KEY= "AIzaSyBXk9t3eACgZy3B6c9lXNYbCl5W4flEUx0";
               
                  node.LocationGranularity = config.LocationGranularity || "none";
                  var selected_item= node.LocationGranularity;

  
              if    (selected_item == "Full Address")
              {  
            
  
                ReducedLocation(LAT, LNG, KEY, (error, LocationData) => {
                      if (error) 
                      {
                        node.error(error, msg);
                         
                      }
                       else 
                       {
                          msg.payload = {FullAddress: LocationData.FullAddress},
                          
                          node.send({
                            topic: "Full Address",
                            payload: msg.payload
                        });   
               
                        }})}
                               
              else if (selected_item == "Post Code")
              { 

            
                ReducedLocation(LAT, LNG, KEY, (error, LocationData) => {
                  if (error) 
                  {
                    node.error(error, msg);
                     
                  }
                   else 
                   {
                          msg.payload = {Post_Code: LocationData.PostCode},
                          node.send({
                            topic: "Post Code",
                            payload: LocationData.PostCode
                        });
             
                        }})}
  
              else if (selected_item == "City Name")
              { 
                ReducedLocation(LAT, LNG, KEY, (error, LocationData) => {
                  if (error) 
                  {
                    node.error(error, msg);
                     
                  }
                   else 
                   {
                          msg.payload = {City_Name: LocationData.CityName},
                          node.send({
                            topic: "City Name",
                            payload: LocationData.CityName
                        });
             
                        }})}
                      
                        else if (selected_item == "Country Name")
                        { 
                          ReducedLocation(LAT, LNG, KEY, (error, LocationData) => {
                            if (error) 
                            {
                              node.error(error, msg);
                               
                            }
                             else 
                             {
                                    msg.payload = {Country_Name: LocationData.CountryName},
                                    node.send({
                                      topic: "Coutry Name",
                                      payload: LocationData.CountryName
                                  });
                       
                                  }})}
                                }
          });
      }
      RED.nodes.registerType("reduce-location-granularity",ReduceGranularityNode);
  }

  
  
