
// to provide the module access to the Node-RED runtime API
module.exports = function(RED) {
  const request = require('request');
  

            function CategoryBasedAggregationNode(config) {
              RED.nodes.createNode(this,config);
              var node = this;
              this.on('input', function(msg) {

                //var HeartRate = msg.payload;

                
                node.SpecifyTyoeofData= config.SpecifyTyoeofData || "none";
                var selected_data = node.SpecifyTyoeofData;

                node.CategoryBasedAggregation= config.CategoryBasedAggregation || "none";
                var selected_Aggregation= node.CategoryBasedAggregation;

                
                //var Aggregator = msg.payload;
                //var Aggregated = [];
                var Aggregat =msg.payload.split(",");  
                var Average = 0;
                var results = 0;
                
            

                for (let i = 0 ; i<Aggregat.length; i++){
                  Average =  Average + parseInt(Aggregat[i]);
                }
                results = Average/Aggregat.length;

                 if  (selected_data == "Activity Spent Time")
                {  
                  Spent_Range = "Activity Spent Time range is between 30 and 20 m ",
                  node.send({
                    topic: "Range of Activity Spent Time",
                    payload: Spent_Range
                });
    
              }
              
              else if (selected_data == "Heart rate") 
                { 
    
                  if (selected_Aggregation == "Status")
                  {

                        if (results >= 60 && results <= 100)
                        {
                          Heart_Status = "Average Heart Rate Status is Normal";
                  
                        }

                        else if (results > 100)
                        {
                          Heart_Status = "Average Heart Rate Status is High";
                        }

                        else if (results < 60)
                        {
                          Heart_Status = "Average Heart Rate Status is Low";
                        }

                        node.send({
                          topic: "Heart Rate Status",
                          payload: Heart_Status
                      });
                  }
    
                
                else if (selected_Aggregation == "Range")
                {
    
                        if (results >= 60 && results <= 100)
                        {
                          Heart_Range = "The range of the Heart Rate Beat is between 60 and 100 BPM";
                        }

                        else if (results > 100)
                        {
                          Heart_Range = "The range of the Heart Rate Beat is greater than 100 BPM" ;
                        }

                        else if (results < 60)
                        {
                          Heart_Range = "The range of the Heart Rate Beat is less than 60 BPM";
                        }

                  node.send({
                    topic: "Range of Heart Rate Beat",
                    payload: Heart_Range
                });
                }
              }
              
    
               else if  (selected_data == "Active calories")
                {  
                  Total_Calories_Range = "Total calories range is between 250 and 300 ",
                  node.send({
                    topic: "Range of Total calories",
                    payload: Total_Calories_Range
                });
    
              }

                  
              else if  (selected_data == "Speed")
              {  
                Speed_Range = "Speed range is between 20 and 44 MPS ",
                node.send({
                  topic: "Range of Speed",
                  payload: Speed_Range
              });

            }
              else if  (selected_data == "Distance")
              {  
                Distacne_Range = "Distance range is between 120 and 160 MS ",
                node.send({
                  topic: "Range of Distance",
                  payload: Distacne_Range
              });
  
            }


    
              });
    }
    RED.nodes.registerType("category-based-aggregation",CategoryBasedAggregationNode);
}

