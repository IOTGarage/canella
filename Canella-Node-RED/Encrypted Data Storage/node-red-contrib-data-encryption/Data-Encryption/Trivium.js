// trivium.js

class Trivium {
    constructor(key, iv) {
        // Initialize the state
        this.state = new Uint8Array(288);
        this.init(key, iv);
    }

    init(key, iv) {
        // Key setup (simplified example)
        for (let i = 0; i < 80; i++) {
            this.state[i] = key[i % key.length];
            this.state[i + 93] = iv[i % iv.length];
        }
        this.state[285] = this.state[286] = this.state[287] = 1;

        // Initial 4 * 288 steps
        for (let i = 0; i < 4 * 288; i++) {
            this.step();
        }
    }

    step() {
        // Trivium algorithm step (simplified example)
        const t1 = this.state[65] ^ (this.state[90] & this.state[91]) ^ this.state[92] ^ this.state[170];
        const t2 = this.state[161] ^ (this.state[174] & this.state[175]) ^ this.state[176] ^ this.state[263];
        const t3 = this.state[242] ^ (this.state[285] & this.state[286]) ^ this.state[287] ^ this.state[68];

        // Shift the state
        for (let i = 287; i > 0; i--) {
            this.state[i] = this.state[i - 1];
        }
        this.state[0] = t3;
        this.state[93] = t1;
        this.state[177] = t2;
    }

    generateKeystream(length) {
        const keystream = new Uint8Array(length);
        for (let i = 0; i < length; i++) {
            this.step();
            keystream[i] = this.state[0] ^ this.state[93] ^ this.state[177] ^ this.state[285];
        }
        return keystream;
    }

    encrypt(plaintext) {
        const plaintextBytes = new TextEncoder().encode(plaintext);
        const keystream = this.generateKeystream(plaintextBytes.length);
        const ciphertextBytes = plaintextBytes.map((byte, i) => byte ^ keystream[i]);
        return Buffer.from(ciphertextBytes).toString('hex');
    }
}

module.exports = Trivium;
