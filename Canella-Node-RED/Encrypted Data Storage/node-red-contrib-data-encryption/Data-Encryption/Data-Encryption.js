module.exports = function(RED) {
    function DataEncryptionNode(config) {
        RED.nodes.createNode(this, config);
        this.SpecifyTyoeofData = config.SpecifyTyoeofData;
        this.DataEncryption = config.DataEncryption;
        var node = this;

        node.on('input', function(msg) {
            var cleartext = msg.payload;
            var ciphertext;

            try {
                if (node.SpecifyTyoeofData === "Symmetric") {
                    switch (node.DataEncryption) {
                        case 'AES (Block Cipher)':
                            const aesjs = require('aes-js');
                            var aesKey = [0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x52, 0x9c, 0x5b, 0xae, 0x8f, 0x82];
                            var textBytes = aesjs.utils.utf8.toBytes(cleartext);
                            var aesCtr = new aesjs.ModeOfOperation.ctr(aesKey, new aesjs.Counter(5));
                            var encryptedBytes = aesCtr.encrypt(textBytes);
                            ciphertext = aesjs.utils.hex.fromBytes(encryptedBytes);
                            break;
                        case 'DES (Block Cipher)':
                            const CryptoJS_DES = require('crypto-js');
                            var desKey = CryptoJS_DES.enc.Hex.parse('133457799BBCDFF1');
                            var desIv = CryptoJS_DES.enc.Hex.parse('0000000000000000'); // DES typically needs an IV
                            var encryptedDES = CryptoJS_DES.DES.encrypt(CryptoJS_DES.enc.Utf8.parse(cleartext), desKey, { iv: desIv, mode: CryptoJS_DES.mode.CBC, padding: CryptoJS_DES.pad.Pkcs7 });
                            ciphertext = encryptedDES.toString();
                            break;
                        case 'Triple DES (Block Cipher)':
                            const CryptoJS_3DES = require('crypto-js');
                            var tripleDesKey = CryptoJS_3DES.enc.Hex.parse('0123456789ABCDEF5555555555555555FFFFFFFFFFFFFFFF');
                            var tripleDesIv = CryptoJS_3DES.enc.Hex.parse('0000000000000000'); // Triple DES typically needs an IV
                            var encrypted3DES = CryptoJS_3DES.TripleDES.encrypt(CryptoJS_3DES.enc.Utf8.parse(cleartext), tripleDesKey, { iv: tripleDesIv, mode: CryptoJS_3DES.mode.CBC, padding: CryptoJS_3DES.pad.Pkcs7 });
                            ciphertext = encrypted3DES.toString();
                            break;
                            case 'Twofish (Block Cipher)':
                                const Twofish = require('./twofish'); // Correct path to your custom Twofish module
                                var twofishKey = Buffer.from('0123456789ABCDEF0123456789ABCDEF', 'hex'); // 256-bit key for Twofish
                                var twofishCipher = new Twofish(twofishKey);
                                ciphertext = twofishCipher.encrypt(cleartext);
                                break;
                        case 'Trivium (Stream Cipher)':
                            const Trivium = require('./trivium'); // Custom implementation path
                            var triviumKey = Buffer.from('0123456789ABCDEF0123456789ABCDEF', 'hex'); // Example key
                            var triviumIV = Buffer.from('00000000000000000000000000000000', 'hex'); // Example IV
                            var trivium = new Trivium(triviumKey, triviumIV);
                            ciphertext = trivium.encrypt(cleartext);
                            break;
                    }
                } else if (node.SpecifyTyoeofData === "Asymmetric") {
                    switch (node.DataEncryption) {
                        case 'RSA':
                            const NodeRSA = require('node-rsa');
                            var rsaKey = new NodeRSA({ b: 2048 }); // Increased key length for security
                            var publicKey = '-----BEGIN PUBLIC KEY-----\nYOUR_PUBLIC_KEY_HERE\n-----END PUBLIC KEY-----'; // Replace with your actual public key in PEM format
                            rsaKey.importKey(publicKey, 'pkcs8-public');
                            ciphertext = rsaKey.encrypt(cleartext, 'base64');
                            break;
                        case 'PKI (Public Key Infrastructure)':
                            const NodeRSA_PKI = require('node-rsa');
                            var pkiKey = new NodeRSA_PKI({ b: 2048 }); // Increased key length for security
                            var pkiPublicKey = '-----BEGIN PUBLIC KEY-----\nYOUR_PUBLIC_KEY_HERE\n-----END PUBLIC KEY-----'; // Replace with your actual public key in PEM format
                            pkiKey.importKey(pkiPublicKey, 'pkcs8-public');
                            ciphertext = pkiKey.encrypt(cleartext, 'base64');
                            break;
                    }
                }
            } catch (err) {
                node.error("Encryption failed: " + err.message);
                return;
            }

            msg.payload = ciphertext;
            node.send(msg);
        });
    }

    RED.nodes.registerType("Data-Encryption", DataEncryptionNode);
};
