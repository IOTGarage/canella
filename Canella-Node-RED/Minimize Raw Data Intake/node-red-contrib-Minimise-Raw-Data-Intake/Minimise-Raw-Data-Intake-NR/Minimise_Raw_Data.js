// to provide the module access to the Node-RED runtime API
module.exports = function(RED) {
  const request = require('request');
  

         /* // To reduce the granularity of location  from the  latitude and longitude coordinates of the location provided
       function Minimise_RawData(Heart_Rate) {
        }*/

//Minimise Raw Data Intake Node

            function MinimiseRawDataIntakeNode(config) {
              RED.nodes.createNode(this,config);
              var node = this;
              this.on('input', function(msg) {


                node.MinimiseRawDataIntake= config.MinimiseRawDataIntake || "none";
                var selected_item= node.MinimiseRawDataIntake;

                var Results = 0;
                var Total = 0;
                
                var Average = msg.payload.split(",");   
                if (selected_item == "Heart rate per minutes values")
                {

                 
                for (let i = 0 ; i<Average.length; i++){
                  Results = Results + parseInt(Average[i]);
                }

                Total = Results/Average.length;

                node.send({
                   topic: "Minimised Heart Rate",
                   payload: String(Total)
                  });

                }

                else if  (selected_item == "Speed per KM values")
                {
                  for (let i = 0 ; i<Average.length; i++){
                    Results = Results + parseFloat(Average[i]);
                    
                  }
                  Total = Results/Average.length;

                  node.send({
                    topic: "Minimised Speed per KM",
                    payload: String(Total)
                   });

                }
            
              });
            }

    RED.nodes.registerType("minimise-raw-data-intake",MinimiseRawDataIntakeNode);
}